﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminCaja.Models
{
    public enum ProcedureFileType
    {
        document, form
    }

    public class CustomerType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ProcedureType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CustomerTypeId { get; set; }
        public virtual CustomerType CustomerType { get; set; }
        public virtual Collection<Procedure> Procedures { get; set; }
    }

    public class ProcedureFile : FileBase
    {
        public int ProcedureId { get; set; }
        public ProcedureFileType Type { get; set; }
        public virtual Procedure Procedure { get; set; }
    }

    public class Procedure
    {
        public int Id { get; set; }
        public int ProcedureTypeId { get; set; }
        public virtual ProcedureType ProcedureType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string WhatIsIt { get; set; }
        public string ForWhom { get; set; }
        public string HowTo { get; set; }
        public string WhereIs { get; set; }
        public string Area { get; set; }
        public virtual Collection<Service> Services { get; set; }
        public virtual Collection<ProcedureFile> Files { get; set; }
    }
}