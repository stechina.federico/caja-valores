﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AdminCaja.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email", ResourceType = typeof(L))]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code", ResourceType = typeof(L))]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?", ResourceType =typeof(L))]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email", ResourceType = typeof(L))]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email", ResourceType = typeof(L))]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(L))]
        public string Password { get; set; }

        [Display(Name = "Rememberme", ResourceType = typeof(L))]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email", ResourceType = typeof(L))]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceName = "Error_Password_StringLength", MinimumLength = 6, ErrorMessageResourceType = typeof(L))]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(L))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password", ResourceType = typeof(L))]
        [Compare("Password", ErrorMessageResourceName = "Error_Password_Match", ErrorMessageResourceType = typeof(L))]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email", ResourceType = typeof(L))]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceName = "Error_Password_StringLength", MinimumLength = 6, ErrorMessageResourceType = typeof(L))]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(L))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password", ResourceType = typeof(L))]
        [Compare("Password", ErrorMessageResourceName = "Error_Password_Compare", ErrorMessageResourceType = typeof(L))]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email", ResourceType = typeof(L))]
        public string Email { get; set; }
    }
}
