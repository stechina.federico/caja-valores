﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminCaja.Models
{
    public class Slide
    {
        public int Id { get; set; }
        public int Order { get; set; }
        public string Title { get; set; }
        public string Drop { get; set; }
        public string Image { get; set; }
        public string ButtonCaption { get; set; }
        public string ButtonUrl { get; set; }
        public string Language { get; set; }

    }
}