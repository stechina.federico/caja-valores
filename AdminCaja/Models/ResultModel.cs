﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminCaja.Models
{
    public class ResultModel
    {
        public ResultModel()
        {
        }
        public ResultModel(Exception exception)
        {
            error = new ErrorResult(exception);
            success = false;
        }
        public bool success { get; set; }
        public ErrorResult error { get; set; }
    }

    public class ResultModelSuccess<T> : ResultModel
    {
        public ResultModelSuccess(): base()
        {
            success = true;
        }
        public T result { get; set; }
    }

    public class ResultModelSuccess : ResultModel
    {
        public ResultModelSuccess() : base()
        {
            success = true;
        }
    }

    public class ResultModelModalResult : ResultModel
    {
        public ResultModelModalResult() : base()
        {
            success = true;
        }

        public string url { get; set; }
        public bool execute { get; set; }
    }

    public class ErrorResult
    {
        public ErrorResult(Exception exception)
        {
            message = exception.Message;
        }
        public string message { get; set; }
    }
}