﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AdminCaja.Models
{
    public class Content
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(25)]
        public string Title { get; set; }

        public string Image { get; set; }
        public bool ContentEnabled { get; set; }
        public string Body { get; set; }
        public string Language { get; set; }
        public virtual Collection<File> Files { get; set; }

        public int SectionId { get; set; }
        public virtual Section Section { get; set; }
    }
}