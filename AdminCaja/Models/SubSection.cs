﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminCaja.Models
{
    public enum SubSectionType
    {
        QueEs, CartaPresidente, Mision, MarcoLegal, MarcoInternacional, Trabajo, Directorio, Organigrama, Codigos, Sustentabilidad, Integridad
         
    }
    public class SubSection
    {
        [Key]
        public int Id { get; set; }

        public int SectionId { get; set; }
        public virtual Section Section { get; set; }

        [MaxLength(50)]
        public string Title { get; set; }
        public string Image { get; set; }
        public string Language { get; set; }
        public SubSectionType Type { get; set; }
    }
}