﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminCaja.Models
{
    public class OtrosServicios
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int Quarter { get; set; }

        public decimal Letters { get; set; }
        public virtual OtrosServiciosData CPD { get; set; }
        public virtual OtrosServiciosData PagareBursatil { get; set; }
    }

    public class OtrosServiciosData
    {
        public int Id { get; set; }
        public OtrosServiciosData()
        {
        }

        public virtual OtrosServiciosDataDetail CasaCentralPesos { get; set; }
        public virtual OtrosServiciosDataDetail CasaCentralUs { get; set; }
        public virtual OtrosServiciosDataDetail SucursalesPesos { get; set; }
        public virtual OtrosServiciosDataDetail SucursalesUs { get; set; }

    }
    public class OtrosServiciosDataDetail
    {
        public int Id { get; set; }

        public int Quantity { get; set; }
        public decimal Amount { get; set; }
    }
}