﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminCaja.Models
{
    public class ActivosEnCustodia
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int Quarter { get; set; }

        public virtual TotalesAEC Totals { get; set; }
        public virtual List<DetailedAEC> Details { get; set; }
    }

    public class TotalesAEC
    {
        public int Id { get; set; }
        public decimal TotalVPN { get; set; }
        public virtual List<FinancialStatesGraph> Graph { get; set; }
    }

    public class DetailedAEC
    {
        public int Id { get; set; }
        public string ColName { get; set; }
        public virtual List<FinancialStatesGraph> Graph { get; set; }

    }

}