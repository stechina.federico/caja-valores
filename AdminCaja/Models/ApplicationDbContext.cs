﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AdminCaja.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public DbSet<Section> Sections { get; set; }
        public DbSet<Content> Content { get; set; }
        public DbSet<SubSection> SubSection { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Slide> Slides { get; set; }
        public DbSet<ProcedureFile> ProcedureFiles { get; set; }
        public DbSet<CustomerType> CustomerTypes { get; set; }
        public DbSet<ProcedureType> ProcedureTypes { get; set; }
        public DbSet<Procedure> Procedures { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<ServiceContent> ServiceContents { get; set; }
        public DbSet<FinancialStatesModel> FinancialStatesModels { get; set; }
        public DbSet<FinancialStates> FinancialStates { get; set; }
        public DbSet<FinancialStatesGraph> FinancialStatesGraph { get; set; }
        public DbSet<OtrosServiciosDataDetail> GetOtrosServiciosDataDetails { get; set; }
        public DbSet<OtrosServiciosData> OtrosServiciosDatas { get; set; }
        public DbSet<OtrosServicios> OtrosServicios { get; set; }
        public DbSet<DepositoInternacional> DepositoInternacional { get; set; }

        public DbSet<TotalesAEC> TotalesAECs { get; set; }
        public DbSet<ActivosEnCustodia> ActivosEnCustodia { get; set; }
        public DbSet<ADCData> ADCData { get; set; }
        public DbSet<ACRyPData> ACRyPData { get; set; }
        public DbSet<RelevantDataGraph> RelevantDataGraph { get; set; }
        public DbSet<RelevantData> RelevantData { get; set; }
    }
}