﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminCaja.Models
{
    public class Service
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string HoverText { get; set; }
        public virtual Collection<Procedure> Procedures { get; set; }
        public virtual Collection<ServiceContent> Contents { get; set; }
    }

    public class ServiceContent
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Language { get; set; }
        public int ServiceId { get; set; }
        public Service Service { get; set; }
    }
}