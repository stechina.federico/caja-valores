﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AdminCaja.Models
{
    public class FinancialStates
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public Decimal SocialCapital { get; set; }
        public Decimal NetWorth { get; set; }
        public virtual List<FinancialStatesGraph> GraphData { get; set; }
    }

    public class FinancialStatesGraph
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
    }

    public class RelevantData
    {
        public int Id { get; set; }

        public int Year { get; set; }
        public int Quarter { get; set; }

        public int SubAccounts { get; set; }
        public virtual List<RelevantDataGraph> GraphData { get; set; }
    }

    public class RelevantDataGraph
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
        public decimal Percent { get; set; }

    }

    public class ACRyPData
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int Quarter { get; set; }

        public int CreditsAmount { get; set; }
        public decimal CreditsPesos { get; set; }
        public int JudicialOffices { get; set; }
        public int RegisteredAccounts { get; set; }
        public int People { get; set; }

    }

    public class ADCData
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int Quarter { get; set; }

        public int SafeVN { get; set; }
        public int SaveAmount { get; set; }
        public int SafeIntl { get; set; }
        public int Kind8Amount { get; set; }
        public int Kind10Amount { get; set; }
        public int Kind7Amount { get; set; }
        public int Kind6Amount { get; set; }

        public decimal Kind8 { get; set; }
        public decimal Kind10 { get; set; }
        public decimal Kind7 { get; set; }
        public decimal Kind6 { get; set; }
    }

    public class DepositoInternacional
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public decimal Euroclear { get; set; }
        public decimal ClearStream { get; set; }
        public decimal DTC { get; set; }
        public decimal CBLC { get; set; }
        public decimal Iberclear { get; set; }
    }

    public class FinancialBalance: FileBase
    {
        public int Year { get; set; }
    }

    public enum FinancialStatesTypeEnum
    {
        Financial, Numbers
    }
    public class FinancialStatesModel
    {
        public int Id { get; set; }
        public FinancialStatesTypeEnum Tipo { get; set; }
        public string Title { get; set; }
        public string HeaderImage { get; set; }
        public string Language { get; set; }

        public virtual Collection<FinancialBalance> Balances { get; set; }
    }
}