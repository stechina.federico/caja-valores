﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminCaja.Models
{
    public class File: FileBase
    {
        public int ContentId { get; set; }
        public virtual Content Content { get; set; }
    }

}