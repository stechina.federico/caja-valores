﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminCaja.Models
{
    public enum SectionType
    {
        Custom, Caja, Gobierno, Comunicados, Novedades
    }

    public class Section
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(25)]
        public string Name { get; set; }
        public bool IsPublic { get; set; }
        public SectionType Type { get; set; } = SectionType.Custom;
        public bool HasContent { get; set; } = true;
        public bool HasDocuments { get; set; } = true;
        public virtual Collection<Content> Contents { get; set; }
        public virtual Collection<SubSection> SubSections { get; set; }
    }

}