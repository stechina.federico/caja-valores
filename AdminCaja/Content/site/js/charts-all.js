if ($(window).width() < 768) {
	AmCharts.makeChart("pie0",
		{
			"type": "pie",
			"balloonText": "[[title]]<br><span style='font-size:14px'><b>([[percents]]%)</b></span>",
			"innerRadius": "70%",
			"baseColor": "",
			"thousandsSeparator": ".",
			"decimalSeparator": ",",
			"colors": [
				
				"#1183d3",
				"#16a8ed",
				"#1bb4b5",
				"#0fbc2e",
				"#8dd537",
				"#04D215",
				"#0D8ECF",
				"#0D52D1",
				"#2A0CD0",
				"#8A0CCF",
				"#CD0D74",
				"#754DEB",
				"#DDDDDD",
				"#999999",
				"#333333",
				"#000000",
				"#57032A",
				"#CA9726",
				"#990000",
				"#4B0C25"
			],
			"labelsEnabled": false,
			"startDuration": 1,
			"startEffect": "easeOutSine",
			"titleField": "Categoria",
			"valueField": "Cantidad",
			"fontFamily": "Montserrat",
			"allLabels": [],
			"balloon": {},
			"legend": {
				"enabled": true,
				"align": "center",
				"autoMargins": false,
				"marginLeft": 2,
				"marginRight": 0,
				"markerLabelGap": 10,
				"markerType": "circle",			
				"rollOverGraphAlpha": 0,
				"valueAlign": "left",
			},
			"titles": [],
			"dataProvider": [
				{
					"Categoria": "Bolsas y Mercados Argentinos S.A. – BYMA",
					"Cantidad": "99.96"
				},			
				{
					"Categoria": "Bolsas de Comercio del Interior",
					"Cantidad": "0.025"
				},
				{
					"Categoria": "Mercado de Valores del Interior",
					"Cantidad": "0.015"
				},
				
			]
		}
	);
}
else {
	AmCharts.makeChart("pie0",
		{
			"type": "pie",
			"balloonText": "[[title]]<br><span style='font-size:14px'><b>([[percents]]%)</b></span>",
			"innerRadius": "70%",
			"baseColor": "",
			"thousandsSeparator": ".",
			"decimalSeparator": ",",
			"colors": [
				
				"#1183d3",
				"#16a8ed",
				"#1bb4b5",
				"#0fbc2e",
				"#8dd537",
				"#04D215",
				"#0D8ECF",
				"#0D52D1",
				"#2A0CD0",
				"#8A0CCF",
				"#CD0D74",
				"#754DEB",
				"#DDDDDD",
				"#999999",
				"#333333",
				"#000000",
				"#57032A",
				"#CA9726",
				"#990000",
				"#4B0C25"
			],
			"labelsEnabled": false,
			"startDuration": 1,
			"startEffect": "easeOutSine",
			"titleField": "Categoria",
			"valueField": "Cantidad",
			"fontFamily": "Montserrat",
			"allLabels": [],
			"balloon": {},
			"legend": {
				"enabled": true,
				"align": "right",
				"autoMargins": false,
				"marginLeft": 2,
				"marginRight": 0,
				"markerLabelGap": 10,
				"markerType": "circle",			
				"position": "right",
				"rollOverGraphAlpha": 0,
				"valueAlign": "left",
				"valueText": "[[value]]%"
			},
			"titles": [],
			"dataProvider": [
				{
					"Categoria": "Bolsas y Mercados Argentinos S.A. – BYMA",
					"Cantidad": "99.96"
				},			
				{
					"Categoria": "Bolsas de Comercio del Interior",
					"Cantidad": "0.025"
				},
				{
					"Categoria": "Mercado de Valores del Interior",
					"Cantidad": "0.015"
				},
			]
		}
	);
}

// Pie 1
if ($(window).width() < 768) {
	AmCharts.makeChart("pie1",
	{
		"type": "pie",
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
		"innerRadius": "70%",
		"baseColor": "",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [
		"#22456c",
		"#1183d3",
		"#16a8ed",
		"#1bb4b5",
		"#0fbc2e",
		"#8dd537",
		"#04D215",
		"#0D8ECF",
		"#0D52D1",
		"#2A0CD0",
		"#8A0CCF",
		"#CD0D74",
		"#754DEB",
		"#DDDDDD",
		"#999999",
		"#333333",
		"#000000",
		"#57032A",
		"#CA9726",
		"#990000",
		"#4B0C25"
		],
		"labelsEnabled": false,
		"startDuration": 1,
		"startEffect": "easeOutSine",
		"titleField": "Participantes directos",
		"valueField": "Cantidad",
		"fontFamily": "Montserrat",
		"allLabels": [],
		"balloon": {},
		"legend": {
			"enabled": true,
			"align": "center",
			"autoMargins": false,
			"marginLeft": 2,
			"marginRight": 0,
			"markerLabelGap": 10,
			"markerType": "circle",			
			"rollOverGraphAlpha": 0,
			"valueAlign": "left",
			"valueText": "[[percents]]%",
		},
		"titles": [],
		"dataProvider": [
		{
			"Participantes directos": "ALyCs",
			"Cantidad": "421"
		},
		{
			"Participantes directos": "Bancos",
			"Cantidad": "129"
		},
		{
			"Participantes directos": "ACDIs",
			"Cantidad": "83"
		},
		{
			"Participantes directos": "Otros",
			"Cantidad": "56"
		},
		{
			"Participantes directos": "Fondos Comunes",
			"Cantidad": "26"
		},
		{
			"Participantes directos": "Compañías Financieras",
			"Cantidad": "13"
		}
		]
	}
	);
}
else {
	AmCharts.makeChart("pie1",
	{
		"type": "pie",
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
		"innerRadius": "70%",
		"baseColor": "",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [
		"#22456c",
		"#1183d3",
		"#16a8ed",
		"#1bb4b5",
		"#0fbc2e",
		"#8dd537",
		"#04D215",
		"#0D8ECF",
		"#0D52D1",
		"#2A0CD0",
		"#8A0CCF",
		"#CD0D74",
		"#754DEB",
		"#DDDDDD",
		"#999999",
		"#333333",
		"#000000",
		"#57032A",
		"#CA9726",
		"#990000",
		"#4B0C25"
		],
		"labelsEnabled": false,
		"startDuration": 1,
		"startEffect": "easeOutSine",
		"titleField": "Participantes directos",
		"valueField": "Cantidad",
		"fontFamily": "Montserrat",
		"allLabels": [],
		"balloon": {},
		"legend": {
			"enabled": true,
			"align": "right",
			"autoMargins": false,
			"marginLeft": 2,
			"marginRight": 0,
			"markerLabelGap": 10,
			"markerType": "circle",			
			"position": "right",
			"rollOverGraphAlpha": 0,
			"valueAlign": "left",
			"valueText": "[[percents]]%",
		},
		"titles": [],
		"dataProvider": [
		{
			"Participantes directos": "ALyCs",
			"Cantidad": "421"
		},
		{
			"Participantes directos": "Bancos",
			"Cantidad": "129"
		},
		{
			"Participantes directos": "ACDIs",
			"Cantidad": "83"
		},
		{
			"Participantes directos": "Otros",
			"Cantidad": "56"
		},
		{
			"Participantes directos": "Fondos Comunes",
			"Cantidad": "26"
		},
		{
			"Participantes directos": "Compañías Financieras",
			"Cantidad": "13"
		}
		]
	}
	);
}

/*
AmCharts.makeChart("pie1",
	{
		"type": "pie",
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
		"innerRadius": "70%",
		"baseColor": "",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [
			"#22456c",
			"#1183d3",
			"#16a8ed",
			"#1bb4b5",
			"#0fbc2e",
			"#8dd537",
			"#04D215",
			"#0D8ECF",
			"#0D52D1",
			"#2A0CD0",
			"#8A0CCF",
			"#CD0D74",
			"#754DEB",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#000000",
			"#57032A",
			"#CA9726",
			"#990000",
			"#4B0C25"
		],
		"labelsEnabled": false,
		"startDuration": 1,
		"startEffect": "easeOutSine",
		"titleField": "Participantes directos",
		"valueField": "Cantidad",
		"fontFamily": "Montserrat",
		"allLabels": [],
		"balloon": {},
		"legend": {
			"enabled": true,
			"align": "right",
			"autoMargins": false,
			"marginLeft": 2,
			"marginRight": 0,
			"markerLabelGap": 10,
			"markerType": "circle",			
			"position": "right",
			"rollOverGraphAlpha": 0,
			"valueAlign": "left",
		},
		"titles": [],
		"dataProvider": [
			{
				"Participantes directos": "ALyCs",
				"Cantidad": "438"
			},
			{
				"Participantes directos": "Bancos",
				"Cantidad": "132"
			},
			{
				"Participantes directos": "ACDIs",
				"Cantidad": "76"
			},
			{
				"Participantes directos": "Otros",
				"Cantidad": "56"
			},
			{
				"Participantes directos": "Fondos Comunes",
				"Cantidad": "27"
			},
			{
				"Participantes directos": "Compañías Financieras",
				"Cantidad": "13"
			}
		]
	}
);
*/

AmCharts.makeChart("barras1",
	{
		"type": "serial",
		"categoryField": "trimestre",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [
			"#e5e5e5",
			"#FCD202",
			"#B0DE09",
			"#0D8ECF",
			"#2A0CD0",
			"#CD0D74",
			"#CC0000",
			"#00CC00",
			"#0000CC",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#990000"
		],
		"startDuration": 1,
		"fontFamily": "Montserrat",
		"categoryAxis": {
			"gridPosition": "start",
			"axisAlpha": 0,
			"axisColor": "#333",
			"gridAlpha": 0,			
		},
		"trendLines": [],
		"graphs": [
			{				
				"colorField": "color",				
				"fillAlphas": 1,
				"id": "AmGraph-1",
				"labelOffset": 4,
				"labelPosition": "middle",
				"labelText": "[[value]]",
				"fontSize": 20,
				"lineColorField": "color",
				"maxBulletSize": 56,
				"minBulletSize": 20,
				"negativeFillAlphas": 0,
				"negativeLineAlpha": 0,
				"title": "graph 1",
				"topRadius": 0,
				"type": "column",
				"valueField": "Cantidad",
				"showBalloon": true,
				"color": "#FFF",
				"boldLabels": true,
				
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"axisAlpha": 0,
				"gridAlpha": 0,
				"minorGridAlpha": 0,
				"title": "",
				"titleBold": false,				
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": [
			{
				"trimestre": "4°T 2017",
				"Cantidad": "340245"
			},
			{
				"trimestre": "1°T 2018",
				"Cantidad": "364146"
			},
			{
				"trimestre": "2°T 2018",
				"Cantidad": "376865"
			},
			{
				"trimestre": "3°T 2018",
				"Cantidad": "405337",
			},
			{
				"trimestre": "4°T 2018",
				"Cantidad": "377832",
				"color": "#16a8ed"
				
			}
		]
	}
);


AmCharts.makeChart("barras3-1",
	{
		"type": "serial",
		"categoryField": "trimestre",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [
			"#e5e5e5",
			"#FCD202",
			"#B0DE09",
			"#0D8ECF",
			"#2A0CD0",
			"#CD0D74",
			"#CC0000",
			"#00CC00",
			"#0000CC",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#990000"
		],
		"startDuration": 1,
		"fontFamily": "Montserrat",
		"categoryAxis": {
			"gridPosition": "start",
			"axisAlpha": 0,
			"axisColor": "#333",
			"gridAlpha": 0,	
		},
		"trendLines": [],
		"graphs": [
			{				
				"colorField": "color",				
				"fillAlphas": 1,
				"id": "AmGraph-1",
				"labelOffset": 4,
				"labelPosition": "middle",
				"labelText": "[[value]]",
				"fontSize": 20,
				"lineColorField": "color",
				"maxBulletSize": 56,
				"minBulletSize": 20,
				"negativeFillAlphas": 0,
				"negativeLineAlpha": 0,
				"title": "graph 1",
				"topRadius": 0,
				"type": "column",
				"valueField": "cantidad",
				"showBalloon": true,
				"color": "#FFF",
				"boldLabels": true,
				"thousandsSeparator": ".",
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"axisAlpha": 0,
				"gridAlpha": 0,
				"minorGridAlpha": 0,
				"title": "",
				"titleBold": false,				
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": [
			
			{
				"trimestre": "4°T 2017",
				"cantidad": "1661293"
			},
			{
				"trimestre": "1°T 2018",
				"cantidad": "1806418"
			},
			{
				"trimestre": "2°T 2018",
				"cantidad": "1887000"
			},
			{
				"trimestre": "3°T 2018",
				"cantidad": "1520000"
			},
			{
				"trimestre": "3°T 2018",
				"cantidad": "1524379",
				"color": "#16a8ed"
				
			}
		]
	}
);

AmCharts.makeChart("barras3-2",
	{
		"type": "serial",
		"categoryField": "trimestre",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [
			"#e5e5e5",
			"#FCD202",
			"#B0DE09",
			"#0D8ECF",
			"#2A0CD0",
			"#CD0D74",
			"#CC0000",
			"#00CC00",
			"#0000CC",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#990000"
		],
		"startDuration": 1,
		"fontFamily": "Montserrat",
		"categoryAxis": {
			"gridPosition": "start",
			"axisAlpha": 0,
			"axisColor": "#333",
			"gridAlpha": 0,			
		},
		"trendLines": [],
		"graphs": [
			{				
				"colorField": "color",				
				"fillAlphas": 1,
				"id": "AmGraph-1",
				"labelOffset": 4,
				"labelPosition": "middle",
				"labelText": "[[value]]",
				"fontSize": 20,
				"lineColorField": "color",
				"maxBulletSize": 56,
				"minBulletSize": 20,
				"negativeFillAlphas": 0,
				"negativeLineAlpha": 0,
				"title": "graph 1",
				"topRadius": 0,
				"type": "column",
				"valueField": "cantidad",
				"showBalloon": true,
				"color": "#FFF",
				"boldLabels": true,
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"axisAlpha": 0,
				"gridAlpha": 0,
				"minorGridAlpha": 0,
				"title": "",
				"titleBold": false,				
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": [
			
			{
				"trimestre": "4°T 2017",
				"cantidad": "3023123"
			},
			{
				"trimestre": "1°T 2018",
				"cantidad": "3428330"
			},
			{
				"trimestre": "2°T 2018",
				"cantidad": "3764811"
			},
			{
				"trimestre": "3°T 2018",
				"cantidad": "4521000"
			},
			{
				"trimestre": "4°T 2018",
				"cantidad": "3931804",
				"color": "#1bb4b5"
				
			}
		]
	}
);

AmCharts.makeChart("barras3-3",
	{
		"type": "serial",
		"categoryField": "trimestre",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [
			"#e5e5e5",
			"#FCD202",
			"#B0DE09",
			"#0D8ECF",
			"#2A0CD0",
			"#CD0D74",
			"#CC0000",
			"#00CC00",
			"#0000CC",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#990000"
		],
		"startDuration": 1,
		"fontFamily": "Montserrat",
		"categoryAxis": {
			"gridPosition": "start",
			"axisAlpha": 0,
			"axisColor": "#333",
			"gridAlpha": 0,			
		},
		"trendLines": [],
		"graphs": [
			{				
				"colorField": "color",				
				"fillAlphas": 1,
				"id": "AmGraph-1",
				"labelOffset": 4,
				"labelPosition": "middle",
				"labelText": "[[value]]",
				"fontSize": 20,
				"lineColorField": "color",
				"maxBulletSize": 56,
				"minBulletSize": 20,
				"negativeFillAlphas": 0,
				"negativeLineAlpha": 0,
				"title": "graph 1",
				"topRadius": 0,
				"type": "column",
				"valueField": "cantidad",
				"showBalloon": true,
				"color": "#FFF",
				"boldLabels": true,
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"axisAlpha": 0,
				"gridAlpha": 0,
				"minorGridAlpha": 0,
				"title": "",
				"titleBold": false,				
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": [
			{
				"trimestre": "3°T 2017",
				"cantidad": "77172"
			},
			{
				"trimestre": "4°T 2017",
				"cantidad": "78459"
			},
			{
				"trimestre": "1°T T 2018",
				"cantidad": "80127"
			},
			{
				"trimestre": "2°T 2018",
				"cantidad": "83535"
			},
			{
				"trimestre": "3°T 2018",
				"cantidad": "87498"
			},
			{
				"trimestre": "4°T 2018",
				"cantidad": "86126",
				"color": "#1fbc2e"
				
			}

		]
	}
);


if ($(window).width() < 768) {
	AmCharts.makeChart("pie4",
	{
		"type": "pie",
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>([[percents]]%)</b></span>",
		"innerRadius": "70%",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"baseColor": "",
		"colors": [
			"#22456c",
			"#1183d3",
			"#16a8ed",
			"#1bb4b5",
			"#0fbc2e",
			"#8dd537",
			"#04D215",
			"#0D8ECF",
			"#0D52D1",
			"#2A0CD0",
			"#8A0CCF",
			"#CD0D74",
			"#754DEB",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#000000",
			"#57032A",
			"#CA9726",
			"#990000",
			"#4B0C25"
		],
		"labelsEnabled": false,
		"startDuration": 1,
		"startEffect": "easeOutSine",
		"titleField": "participacion",
		"valueField": "porcentaje",
		"fontFamily": "Montserrat",
		"allLabels": [],
		"balloon": {},
		"legend": {
			"enabled": true,
			"align": "center",
			"autoMargins": false,
			"marginLeft": 2,
			"marginRight": 0,
			"markerLabelGap": 10,
			"markerType": "circle",			
			"rollOverGraphAlpha": 0,
			"valueAlign": "left",
			"valueText": "[[value]]%",
		},
		"titles": [],
		"dataProvider": [
			{
				"participacion": "Títulos Públicos",
				"porcentaje": "83.18"
			},
			{
				"participacion": "Fideicomisos Financieros",
				"porcentaje": "6.98"
			},
			{
				"participacion": "Obligaciones Negociables",
				"porcentaje": "8.33"
			},
			{
				"participacion": "Acciones",
				"porcentaje": "0.96"
			},
			{
				"participacion": "Monedas",
				"porcentaje": "0.19"
			},
			{
				"participacion": "Fondos de Inversión",
				"porcentaje": "0.35"
			},
			{
				"participacion": "ADRS",
				"porcentaje": "0.002"
			},
			{
				"participacion": "CEDEARS",
				"porcentaje": "0.004"
			}
		]
	}
);
}
else {
	AmCharts.makeChart("pie4",
	{
		"type": "pie",
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>([[percents]]%)</b></span>",
		"innerRadius": "70%",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"baseColor": "",
		"colors": [
			"#22456c",
			"#1183d3",
			"#16a8ed",
			"#1bb4b5",
			"#0fbc2e",
			"#8dd537",
			"#04D215",
			"#0D8ECF",
			"#0D52D1",
			"#2A0CD0",
			"#8A0CCF",
			"#CD0D74",
			"#754DEB",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#000000",
			"#57032A",
			"#CA9726",
			"#990000",
			"#4B0C25"
		],
		"labelsEnabled": false,
		"startDuration": 1,
		"startEffect": "easeOutSine",
		"titleField": "participacion",
		"valueField": "porcentaje",
		"fontFamily": "Montserrat",
		"allLabels": [],
		"balloon": {},
		"legend": {
			"enabled": true,
			"align": "right",
			"autoMargins": false,
			"marginLeft": 2,
			"marginRight": 0,
			"markerLabelGap": 10,
			"markerType": "circle",			
			"position": "right",
			"rollOverGraphAlpha": 0,
			"valueAlign": "left",
			"valueText": "[[value]]%",
		},
		"titles": [],
		"dataProvider": [
			{
				"participacion": "Títulos Públicos",
				"porcentaje": "83.18"
			},
			{
				"participacion": "Fideicomisos Financieros",
				"porcentaje": "6.98"
			},
			{
				"participacion": "Obligaciones Negociables",
				"porcentaje": "8.33"
			},
			{
				"participacion": "Acciones",
				"porcentaje": "0.96"
			},
			{
				"participacion": "Monedas",
				"porcentaje": "0.19"
			},
			{
				"participacion": "Fondos de Inversión",
				"porcentaje": "0.35"
			},
			{
				"participacion": "ADRS",
				"porcentaje": "0.002"
			},
			{
				"participacion": "CEDEARS",
				"porcentaje": "0.004"
			}
		]
	}
);
}


AmCharts.makeChart("pie4-1",
	{
		"type": "pie",
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>([[percents]]%)</b></span>",
		"innerRadius": "60%",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"baseColor": "",
		"colors": [			
			"#1183d3",
			"#1bb4b5",
			"#1fbc2e",
			"#22456c",
			"#04D215",
			"#0D8ECF",
			"#0D52D1",
			"#2A0CD0",
			"#8A0CCF",
			"#CD0D74",
			"#754DEB",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#000000",
			"#57032A",
			"#CA9726",
			"#990000",
			"#4B0C25"
		],
		"labelsEnabled": true,
		"startDuration": 1,
		"startEffect": "easeOutSine",
		"titleField": "category",
		"valueField": "column-1",
		"fontFamily": "Montserrat",
		"allLabels": [],
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]%</b></span>",
		"legend": false,
		"titles": [],
		"dataProvider": [
			{
				"category": "Persona Física Argentina",
				"column-1": "26.5"
			},
			{
				"category": "Persona Física Exterior",
				"column-1": "0.17"
			},
			{
				"category": "Persona Jurídica Argentina",
				"column-1": "48.54"
			},
			{
				"category": "Persona Jurídica Exterior",
				"column-1": "24.79"
			}			
		]
	}
);

AmCharts.makeChart("pie4-2",
	{
		"type": "pie",
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]%</b></span>",
		"innerRadius": "60%",
		"baseColor": "",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [			
			"#1183d3",
			"#1bb4b5",
			"#1fbc2e",
			"#22456c",
			"#04D215",
			"#0D8ECF",
			"#0D52D1",
			"#2A0CD0",
			"#8A0CCF",
			"#CD0D74",
			"#754DEB",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#000000",
			"#57032A",
			"#CA9726",
			"#990000",
			"#4B0C25"
		],
		"labelsEnabled": true,
		"startDuration": 1,
		"startEffect": "easeOutSine",
		"titleField": "category",
		"valueField": "column-1",
		"fontFamily": "Montserrat",
		"allLabels": [],
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]%</b></span>",
		"legend": false,
		"titles": [],
		"dataProvider": [
			{
				"category": "Persona Física Argentina",
				"column-1": "2.9"
			},
			{
				"category": "Persona Física Exterior",
				"column-1": "0.01"
			},
			{
				"category": "Persona Jurídica Argentina",
				"column-1": "37"
			},
			{
				"category": "Persona Jurídica Exterior",
				"column-1": "60.09"
			}			
		]
	}
);

AmCharts.makeChart("pie4-3",
	{
		"type": "pie",
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]%</b></span>",
		"innerRadius": "60%",
		"baseColor": "",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [			
			"#1183d3",
			"#1bb4b5",
			"#1fbc2e",
			"#22456c",
			"#04D215",
			"#0D8ECF",
			"#0D52D1",
			"#2A0CD0",
			"#8A0CCF",
			"#CD0D74",
			"#754DEB",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#000000",
			"#57032A",
			"#CA9726",
			"#990000",
			"#4B0C25"
		],
		"labelsEnabled": true,
		"startDuration": 1,
		"startEffect": "easeOutSine",
		"titleField": "category",
		"valueField": "column-1",
		"fontFamily": "Montserrat",
		"allLabels": [],
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]%</b></span>",
		"legend": false,
		"titles": [],
		"dataProvider": [
			{
				"category": "Persona Física Argentina",
				"column-1": "1.01"
			},
			{
				"category": "Persona Física Exterior",
				"column-1": "0"
			},
			{
				"category": "Persona Jurídica Argentina",
				"column-1": "91.03"
			},
			{
				"category": "Persona Jurídica Exterior",
				"column-1": "7.96"
			}			
		]
	}
);

AmCharts.makeChart("pie4-4",
	{
		"type": "pie",
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]%</b></span>",
		"innerRadius": "60%",
		"baseColor": "",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [			
			"#1183d3",
			"#1bb4b5",
			"#1fbc2e",
			"#22456c",
			"#04D215",
			"#0D8ECF",
			"#0D52D1",
			"#2A0CD0",
			"#8A0CCF",
			"#CD0D74",
			"#754DEB",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#000000",
			"#57032A",
			"#CA9726",
			"#990000",
			"#4B0C25"
		],
		"labelsEnabled": true,
		"startDuration": 1,
		"startEffect": "easeOutSine",
		"titleField": "category",
		"valueField": "column-1",
		"fontFamily": "Montserrat",
		"allLabels": [],
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]%</b></span>",
		"legend": false,
		"titles": [],
		"dataProvider": [
			{
				"category": "Persona Física Argentina",
				"column-1": "27.54"
			},
			{
				"category": "Persona Física Exterior",
				"column-1": "0.14"
			},
			{
				"category": "Persona Jurídica Argentina",
				"column-1": "71.3"
			},
			{
				"category": "Persona Jurídica Exterior",
				"column-1": "1.02"
			}			
		]
	}
);

AmCharts.makeChart("pie4-5",
	{
		"type": "pie",
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]%</b></span>",
		"innerRadius": "60%",
		"baseColor": "",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [			
			"#1183d3",
			"#1bb4b5",
			"#1fbc2e",
			"#22456c",
			"#04D215",
			"#0D8ECF",
			"#0D52D1",
			"#2A0CD0",
			"#8A0CCF",
			"#CD0D74",
			"#754DEB",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#000000",
			"#57032A",
			"#CA9726",
			"#990000",
			"#4B0C25"
		],
		"labelsEnabled": true,
		"startDuration": 1,
		"startEffect": "easeOutSine",
		"titleField": "category",
		"valueField": "column-1",
		"fontFamily": "Montserrat",
		"allLabels": [],
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]%</b></span>",
		"legend": false,
		"titles": [],
		"dataProvider": [
			{
				"category": "Persona Física Argentina",
				"column-1": "33.24"
			},
			{
				"category": "Persona Física Exterior",
				"column-1": "0.05"
			},
			{
				"category": "Persona Jurídica Argentina",
				"column-1": "65.02"
			},
			{
				"category": "Persona Jurídica Exterior",
				"column-1": "1.69"
			}			
		]
	}
);

AmCharts.makeChart("pie4-6",
	{
		"type": "pie",
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]%</b></span>",
		"innerRadius": "60%",
		"baseColor": "",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [			
			"#1183d3",
			"#1bb4b5",
			"#1fbc2e",
			"#22456c",
			"#04D215",
			"#0D8ECF",
			"#0D52D1",
			"#2A0CD0",
			"#8A0CCF",
			"#CD0D74",
			"#754DEB",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#000000",
			"#57032A",
			"#CA9726",
			"#990000",
			"#4B0C25"
		],
		"labelsEnabled": true,
		"startDuration": 1,
		"startEffect": "easeOutSine",
		"titleField": "category",
		"valueField": "column-1",
		"fontFamily": "Montserrat",
		"allLabels": [],
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]%</b></span>",
		"legend": false,
		"titles": [],
		"dataProvider": [
			{
				"category": "Persona Física Argentina",
				"column-1": "1.65"
			},
			{
				"category": "Persona Física Exterior",
				"column-1": "0.01"
			},
			{
				"category": "Persona Jurídica Argentina",
				"column-1": "98.23"
			},
			{
				"category": "Persona Jurídica Exterior",
				"column-1": "0.1"
			}			
		]
	}
);

AmCharts.makeChart("pie4-7",
	{
		"type": "pie",
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]%</b></span>",
		"innerRadius": "60%",
		"baseColor": "",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [			
			"#1183d3",
			"#1bb4b5",
			"#1fbc2e",
			"#22456c",
			"#04D215",
			"#0D8ECF",
			"#0D52D1",
			"#2A0CD0",
			"#8A0CCF",
			"#CD0D74",
			"#754DEB",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#000000",
			"#57032A",
			"#CA9726",
			"#990000",
			"#4B0C25"
		],
		"labelsEnabled": true,
		"startDuration": 1,
		"startEffect": "easeOutSine",
		"titleField": "category",
		"valueField": "column-1",
		"fontFamily": "Montserrat",
		"allLabels": [],
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]%</b></span>",
		"legend": false,
		"titles": [],
		"dataProvider": [
			{
				"category": "Persona Física Argentina",
				"column-1": "21.32"
			},
			{
				"category": "Persona Física Exterior",
				"column-1": "0.04"
			},
			{
				"category": "Persona Jurídica Argentina",
				"column-1": "78.63"
			},
			{
				"category": "Persona Jurídica Exterior",
				"column-1": "0.01"
			}			
		]
	}
);

AmCharts.makeChart("pie4-8",
	{
		"type": "pie",
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]%</b></span>",
		"innerRadius": "60%",
		"baseColor": "",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [			
			"#1183d3",
			"#1bb4b5",
			"#1fbc2e",
			"#22456c",
			"#04D215",
			"#0D8ECF",
			"#0D52D1",
			"#2A0CD0",
			"#8A0CCF",
			"#CD0D74",
			"#754DEB",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#000000",
			"#57032A",
			"#CA9726",
			"#990000",
			"#4B0C25"
		],
		"labelsEnabled": true,
		"startDuration": 1,
		"startEffect": "easeOutSine",
		"titleField": "category",
		"valueField": "column-1",
		"fontFamily": "Montserrat",
		"allLabels": [],
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]%</b></span>",
		"legend": false,
		"titles": [],
		"dataProvider": [
			{
				"category": "Persona Física Argentina",
				"column-1": "7.84"
			},
			{
				"category": "Persona Física Exterior",
				"column-1": "0.04"
			},
			{
				"category": "Persona Jurídica Argentina",
				"column-1": "50.1"
			},
			{
				"category": "Persona Jurídica Exterior",
				"column-1": "42.02"
			}			
		]
	}
);

AmCharts.makeChart("barras5-1",
	{
		"type": "serial",
		"categoryField": "category",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [
			"#e5e5e5",
			"#FCD202",
			"#B0DE09",
			"#0D8ECF",
			"#2A0CD0",
			"#CD0D74",
			"#CC0000",
			"#00CC00",
			"#0000CC",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#990000"
		],
		"startDuration": 1,
		"fontFamily": "Montserrat",
		"categoryAxis": {
			"gridPosition": "start",
			"axisAlpha": 0,
			"axisColor": "#333",
			"gridAlpha": 0,			
		},
		"trendLines": [],
		"graphs": [
			{				
				"colorField": "color",				
				"fillAlphas": 1,
				"id": "AmGraph-1",
				"labelOffset": 4,
				"labelPosition": "middle",
				"labelText": "[[value]]",
				"lineColorField": "color",
				"maxBulletSize": 56,
				"minBulletSize": 20,
				"negativeFillAlphas": 0,
				"negativeLineAlpha": 0,
				"title": "graph 1",
				"topRadius": 0,
				"type": "column",
				"valueField": "column-1",
				"showBalloon": true,
				"color": "#FFF",
				"boldLabels": true,
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"axisAlpha": 0,
				"gridAlpha": 0,
				"minorGridAlpha": 0,
				"title": "",
				"titleBold": false,				
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": [
			{
				"category": "2013",
				"column-1": "13091",
				"color": "#001f5f"
			},
			{
				"category": "2014",
				"column-1": "14088",
				"color": "#0183d9"
			},
			{
				"category": "2015",
				"column-1": "10461",
				"color": "#00a8f1"
			},
			{
				"category": "2016",
				"column-1": "50738",
				"color": "#00b6b8"
			},
			{
				"category": "2017",
				"column-1": "57096",
				"color": "#00c000"
				
			},
			{
				"category": "2018",
				"column-1": "61563",
				"color": "#8dd906"
			}
		]
	}
);

AmCharts.makeChart("barras5-2",
	{
		"type": "serial",
		"categoryField": "category",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [
			"#e5e5e5",
			"#FCD202",
			"#B0DE09",
			"#0D8ECF",
			"#2A0CD0",
			"#CD0D74",
			"#CC0000",
			"#00CC00",
			"#0000CC",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#990000"
		],
		"startDuration": 1,
		"fontFamily": "Montserrat",
		"categoryAxis": {
			"gridPosition": "start",
			"axisAlpha": 0,
			"axisColor": "#333",
			"gridAlpha": 0,			
		},
		"trendLines": [],
		"graphs": [
			{				
				"colorField": "color",				
				"fillAlphas": 1,
				"id": "AmGraph-1",
				"labelOffset": 4,
				"labelPosition": "middle",
				"labelText": "[[value]]",
				
				"lineColorField": "color",
				"maxBulletSize": 56,
				"minBulletSize": 20,
				"negativeFillAlphas": 0,
				"negativeLineAlpha": 0,
				"title": "graph 1",
				"topRadius": 0,
				"type": "column",
				"valueField": "column-1",
				"showBalloon": true,
				"color": "#FFF",
				"boldLabels": true,
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"axisAlpha": 0,
				"gridAlpha": 0,
				"minorGridAlpha": 0,
				"title": "",
				"titleBold": false,				
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": [
			{
				"category": "2013",
				"column-1": "1220",
				"color": "#001f5f"
			},
			{
				"category": "2014",
				"column-1": "1166",
				"color": "#0183d9"
			},
			{
				"category": "2015",
				"column-1": "1175",
				"color": "#00a8f1"
			},
			{
				"category": "2016",
				"column-1": "17634",
				"color": "#00b6b8"
			},
			{
				"category": "2017",
				"column-1": "22669",
				"color": "#00c000"
				
			},
			{
				"category": "2018",
				"column-1": "24157",
				"color": "#8dd906"
				
			}
		]
	}
);

AmCharts.makeChart("barras5-3",
	{
		"type": "serial",
		"categoryField": "category",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [
			"#e5e5e5",
			"#FCD202",
			"#B0DE09",
			"#0D8ECF",
			"#2A0CD0",
			"#CD0D74",
			"#CC0000",
			"#00CC00",
			"#0000CC",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#990000"
		],
		"startDuration": 1,
		"fontFamily": "Montserrat",
		"categoryAxis": {
			"gridPosition": "start",
			"axisAlpha": 0,
			"axisColor": "#333",
			"gridAlpha": 0,			
		},
		"trendLines": [],
		"graphs": [
			{				
				"colorField": "color",				
				"fillAlphas": 1,
				"id": "AmGraph-1",
				"labelOffset": 4,
				"labelPosition": "middle",
				"labelText": "[[value]]",				
				"lineColorField": "color",
				"maxBulletSize": 56,
				"minBulletSize": 20,
				"negativeFillAlphas": 0,
				"negativeLineAlpha": 0,
				"title": "graph 1",
				"topRadius": 0,
				"type": "column",
				"valueField": "column-1",
				"showBalloon": true,
				"color": "#FFF",
				"boldLabels": true,
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"axisAlpha": 0,
				"gridAlpha": 0,
				"minorGridAlpha": 0,
				"title": "",
				"titleBold": false,				
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": [
			
			{
				"category": "2013",
				"column-1": "1094",
				"color": "#001f5f"
			},
			{
				"category": "2014",
				"column-1": "197",
				"color": "#0183d9"
			},
			{
				"category": "2015",
				"column-1": "112",
				"color": "#00a8f1"
			},
			{
				"category": "2016",
				"column-1": "282",
				"color": "#00b6b8"
			},
			{
				"category": "2017",
				"column-1": "308",
				"color": "#00c000"
				
			},
			{
				"category": "2018",
				"column-1": "370",
				"color": "#8dd906"
				
			}
		]
	}
);

AmCharts.makeChart("barras5-4",
	{
		"type": "serial",
		"categoryField": "category",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [
			"#e5e5e5",
			"#FCD202",
			"#B0DE09",
			"#0D8ECF",
			"#2A0CD0",
			"#CD0D74",
			"#CC0000",
			"#00CC00",
			"#0000CC",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#990000"
		],
		"startDuration": 1,
		"fontFamily": "Montserrat",
		"categoryAxis": {
			"gridPosition": "start",
			"axisAlpha": 0,
			"axisColor": "#333",
			"gridAlpha": 0,			
		},
		"trendLines": [],
		"graphs": [
			{				
				"colorField": "color",				
				"fillAlphas": 1,
				"id": "AmGraph-1",
				"labelOffset": 4,
				"labelPosition": "middle",
				"labelText": "[[value]]",
				
				"lineColorField": "color",
				"maxBulletSize": 56,
				"minBulletSize": 20,
				"negativeFillAlphas": 0,
				"negativeLineAlpha": 0,
				"title": "graph 1",
				"topRadius": 0,
				"type": "column",
				"valueField": "column-1",
				"showBalloon": true,
				"color": "#FFF",
				"boldLabels": true,
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"axisAlpha": 0,
				"gridAlpha": 0,
				"minorGridAlpha": 0,
				"title": "",
				"titleBold": false,				
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": [
			{
				"category": "2013",
				"column-1": "12",
				"color": "#001f5f"
			},
			{
				"category": "2014",
				"column-1": "29",
				"color": "#0183d9"
			},
			{
				"category": "2015",
				"column-1": "69",
				"color": "#00a8f1"
			},
			{
				"category": "2016",
				"column-1": "42",
				"color": "#00b6b8"
			},
			{
				"category": "2017",
				"column-1": "44",
				"color": "#00c000"
			},
			{
				"category": "2018",
				"column-1": "29",
				"color": "#8dd906"
				
			}
		]
	}
);

AmCharts.makeChart("barras5-5",
	{
		"type": "serial",
		"categoryField": "category",
		"thousandsSeparator": ".",
		"decimalSeparator": ",",
		"colors": [
			"#e5e5e5",
			"#FCD202",
			"#B0DE09",
			"#0D8ECF",
			"#2A0CD0",
			"#CD0D74",
			"#CC0000",
			"#00CC00",
			"#0000CC",
			"#DDDDDD",
			"#999999",
			"#333333",
			"#990000"
		],
		"startDuration": 1,
		"fontFamily": "Montserrat",
		"categoryAxis": {
			"gridPosition": "start",
			"axisAlpha": 0,
			"axisColor": "#333",
			"gridAlpha": 0,			
		},
		"trendLines": [],
		"graphs": [
			{				
				"colorField": "color",				
				"fillAlphas": 1,
				"id": "AmGraph-1",
				"labelOffset": 4,
				"labelPosition": "middle",
				"labelText": "[[value]]",
				"lineColorField": "color",
				"maxBulletSize": 56,
				"minBulletSize": 20,
				"negativeFillAlphas": 0,
				"negativeLineAlpha": 0,
				"title": "graph 1",
				"topRadius": 0,
				"type": "column",
				"valueField": "column-1",
				"showBalloon": true,
				"color": "#FFF",
				"boldLabels": true,
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"axisAlpha": 0,
				"gridAlpha": 0,
				"minorGridAlpha": 0,
				"title": "",
				"titleBold": false,				
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": [
			{
				"category": "2013",
				"column-1": "9",
				"color": "#001f5f"
			},
			{
				"category": "2014",
				"column-1": "9",
				"color": "#0183d9"
			},
			{
				"category": "2015",
				"column-1": "9",
				"color": "#00a8f1"
			},
			{
				"category": "2016",
				"column-1": "10",
				"color": "#00b6b8"
			},
			{
				"category": "2017",
				"column-1": "10",
				"color": "#00c000"
				
			},
			{
				"category": "2018",
				"column-1": "8",
				"color": "#8dd906"
				
			}
		]
	}
);