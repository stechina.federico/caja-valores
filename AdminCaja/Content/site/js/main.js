
// PRELOADER

$(window).on('load', function() { 
	$('.loader').delay(500).removeClass('loader'); // will first fade out the loading animation 
});


// ACTIVO TOOLTIPS

$('.has-tooltip').tooltip();


// MUESTRO Y OCULTO HEADER EN SCROLL

var position = $(window).scrollTop(); 
$(window).scroll(function() {
	var scroll = $(window).scrollTop();
	if(scroll > position && scroll > 150) {
		$('header').addClass('header-hide header-scroll');
	} else {
		$('header').removeClass('header-hide');
	}
	if(scroll < 150) {
		$('header').removeClass('header-scroll');
	}	
	position = scroll;
});


// MUESTRO Y OCULTO HEADER EN SCROLL

$('.main-nav .submenu').click(function() {
	$(this).toggleClass('open').next().slideToggle(500);
});

$('.main-nav .submenu-2').click(function() {
	$(this).toggleClass('open').next().slideToggle(500);
});



// MENÚ DESPLEGABLE

$('.open-nav').click(function() {
	$(this).toggleClass('close-nav');
	$('body').toggleClass('main-nav-open');
});

$(function () {    
  $('.main-nav2 li').hover(function () {
	 clearTimeout($.data(this, 'timer'));
	 $('ul', this).stop(true, true).slideDown(200);
  }, function () {
	$.data(this, 'timer', setTimeout($.proxy(function() {
	  $('ul', this).stop(true, true).slideUp(200);
	}, this), 100));
  });
});

// TITULO DESPLEGABLE DE HERRAMIENTAS

$('.titulo-desplegable > a').click(function() {
	$(this).parent().toggleClass('titulo-desplegado')
	$(this).next('ul').fadeToggle(300)
	$(this).find('i').toggleClass('icon-chevron-up');
});

$(document).click(function() { 
	if(!$(event.target).closest('.titulo-desplegable').length) {
		$('.titulo-desplegable').removeClass('titulo-desplegado')
		$('.titulo-desplegable ul').fadeOut(300);
		$('.titulo-desplegable i').removeClass('icon-chevron-up');
	}
});

$('.titulo-desplegable-fondos').click(function() {
	$(this).find('i').toggleClass('icon-chevron-up');
	$('.desplegable-fondos').fadeToggle(300)
});


// BTN desplegable servicios
/*
$(".btn-desplegable").mouseenter(function() {
	$(this).find('.list-show').fadeIn(200);
}).mouseleave(function() {
	$(this).find('.list-show').fadeOut(200);
});
*/
// CONTADOR ANIMADO DE NÚMEROS

function countUp() {
	$('.count').each(function() {
		$(this).prop('Counter', 0).animate({
		Counter: $(this).text()
		}, {
		duration: 3000,
		easing: 'swing',
		step: function(now) {
			$(this).text(Math.ceil(now));
		}
		});
	});
}

$(function () {
    "user strict";
    var bAnimate = true;
    $(".count").css("opacity", "0.0");

    $(".count").css("opacity", "1.0").css("fontWeight", "400");
    countUp();
});

// DIBUJADO ANIMADO DE ICONOS

var els = document.getElementsByClassName("icon-fill");
for (var i = els.length - 1; i >= 0; i--) {
	new Vivus(els[i], { type: 'delayed', duration: 100 });
}

// TEXTO EN SELECT

$(document).ready(function () {
	$('.dropdown-item').click(function () {
        var texto = $(this).html();
        if (texto.length > 33) {
            $(this).parent().prev('.dropdown-toggle').html(texto.substr(0, 30) + '...')
        } else {
            $(this).parent().prev('.dropdown-toggle').html(texto);
        }        
    });
});

// DESPLEGAR TRAMITES

$(document).ready(function () {
	$('.expand-button').on('click', function(){
	  $('.container-tramites').toggleClass('-expanded');
	  
	  if ($('.container-tramites').hasClass('-expanded')) {
	    $('.expand-button').html('MOSTRAR MENOS <i class="icon-chevron-up">');
	  } else {
	    $('.expand-button').html('MOSTRAR MÁS <i class="icon-chevron-down">');
	  }
	});
});