﻿// Uso:
// Definir un div placeHolder <div data-modal-place="groupName"></div>
//          Es el div donde se colocará la ventana modal mediante ajax
// Definir un desencadenador <a href="yyyy" data-modal="groupName" >link</a>
//          Es el link al contenido que se mostrará en modal
// Definir un div para el contenido resultante del modal
//          <div data-modal-taget="groupName"></div>

"use strict";

$(function () {

    if (modalFormInitialized) return;
    var modalFormInitialized = true;

    $.ajaxSetup({ cache: false });

    $("div[data-modal-place]").each(function () {
        var mydiv = $(this);
        var modalContentName = this.getAttribute('data-modal-place') + 'ModalContent';
        var modalName = this.getAttribute('data-modal-place') + 'ModalDiv';

        mydiv.html("<div class=\"modal fade\" id='" + modalName + "' tabindex=\"-1\" role=\"dialog\"><div class=\"modal-dialog\" role=\"document\"><div class=\"modal-content\"><div id='" + modalContentName + "'></div></div></div></div>");
        //mydiv.addClass('modal fade in');
        var clases = this.getAttribute('data-modal-class');

        $(".modal-dialog").addClass(clases);

    });
    $(document).off("click", "a[data-modal]");
    $(document).on("click", "a[data-modal]", bindclick);
});

function bindclick(e) {
    // hide dropdown if any (this is used wehen invoking modal from link in bootstrap dropdown )
    //$(e.target).closest('.btn-group').children('.dropdown-toggle').dropdown('toggle');
    var key = this.getAttribute('data-modal');
    var modaldiv = "*[data-modal-place=\"" + key + "\"]";
    var modalcontentdiv = "#" + key + "ModalContent";
    var modalName = "#" + key  + 'ModalDiv';

    $(modalcontentdiv).load(this.href, function () {
        $(modalName).modal({
            /*backdrop: 'static',*/
            keyboard: true
        }, 'show');
        bindForm(this, key);
    });
    
    return false;
}

function bindForm(dialog, key) {
    var form = $('form', dialog);

    try {
        if ($.validator && $.validator.unobtrusive)
            $.validator.unobtrusive.parse(form);
    } catch (e) {
        console.log("Error en Parse",e);
    }
    
    form.submit(function (event) {

        if (this.submited) return false;
        this.submited = true;

        event.stopImmediatePropagation();
        var modaldiv = "*[data-modal-place=\"" + key + "\"]";
        var target = "*[data-modal-target=\"" + key + "\"]";
        var modalcontentdiv = "#" + key + "ModalContent";
        var modalName = "#" + key + 'ModalDiv';

        var savekey = key;
        console.log("form submit", this.action);
            $.ajax({
                url: this.action,
                type: this.method,
                data: new FormData(this), //$(this).serialize(),
                contentType: false,
                processData: false,
                success: function (result) {
                    if (result.execute === true) {
                        modalFormExecute(key, result);
                        $(modalName).modal('hide');
                    } else {
                        if (result.success) {
                            $(modalName).modal('hide');
                            if(result.url) $(target).load(result.url); //  Load data from the server and place the returned HTML into the matched element
                            $(target).trigger("onload");
                        } else {
                            $(modalcontentdiv).html(result);
                            bindForm(dialog, savekey);
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error ajax", textStatus, errorThrown);
                },
                statusCode: { 401: function () { location.reload(true); } },
                complete: function () { if (typeof init === 'function') init();}
            });

        return false;
    });
}