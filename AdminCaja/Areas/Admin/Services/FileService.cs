﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace AdminCaja.Areas.Admin.Services
{
    public interface IFileService
    {
        string SaveFile(HttpPostedFileBase file);
        void DeleteImage(string url);
    }
    public class FileService : IFileService
    {
        public void DeleteImage(string url)
        {
            var path = HostingEnvironment.MapPath(url);

            if (File.Exists(url))
            {
                File.Delete(url);
            }
        }

        public string SaveFile(HttpPostedFileBase file)
        {
            var virtualPath = "/images/Upload/";
            var path = HostingEnvironment.MapPath(virtualPath);

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            var fileName = Path.Combine(path, file.FileName);
            if (File.Exists(fileName))
                File.Delete(fileName);

            file.SaveAs(fileName);
            return virtualPath + file.FileName;
        }
    }
}