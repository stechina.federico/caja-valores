﻿using AdminCaja.Areas.Admin.Models;
using AdminCaja.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace AdminCaja.Areas.Admin.Services
{
    public interface ISectionService
    {
        Task<PaggedData<SectionListViewModel>> GetSectionsAsync(int Page, int itemsPerPage);
        Task<Section> SaveSectionAsync(Section section);
        Task<Section> GetSectionAsync(int id);
        Task<Content> SaveContentAsync(Content content);
        Task<Content> GetContentAsync(int id);
    }
    public class SectionService : ISectionService
    {
        private ApplicationDbContext _context;
        private IMapper _mapper;

        public SectionService(ApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _context = applicationDbContext;
            _mapper = mapper;
        }

        public async Task<Content> GetContentAsync(int id)
        {
            var content = await _context.Content
                .Include(_=> _.Files)
                .Where(_=> _.Id == id)
                .FirstOrDefaultAsync();

            return content;
        }

        public async Task<Section> GetSectionAsync(int id)
        {
            var section = await _context.Sections
                            .Include(_=> _.Contents)
                            .Where(_=> _.Id == id)
                            .FirstOrDefaultAsync();

            return section;
        }

        public async Task<PaggedData<SectionListViewModel>> GetSectionsAsync(int Page, int itemsPerPage)
        {
            PaggedData<SectionListViewModel> result = new PaggedData<SectionListViewModel>();

            result.Items = await _context.Sections.CountAsync();
            result.ItemsPerPage = itemsPerPage;
            result.CurrentPage = Page;

            var sections = await _context.Sections
                .OrderBy(x=> x.Id)
                .Skip(result.ItemsPerPage * (result.CurrentPage -1))
                .Take(result.ItemsPerPage)
                .ToListAsync();

            result.Data = _mapper.Map<List<Section>, List<SectionListViewModel>>(sections);

            return result;
        }

        public async Task<Content> SaveContentAsync(Content content)
        {
            var savedContent = await _context.Content.FindAsync(content.Id);
            if(savedContent == null)
            {
                _context.Content.Add(content);
                savedContent = content;
            }
            else
            {
                _mapper.Map(content, savedContent);
            }

            await _context.SaveChangesAsync();
            return savedContent;
        }

        public async Task<Section> SaveSectionAsync(Section section)
        {
            var savedSection = await _context.Sections.FindAsync(section.Id);

            if (savedSection == null)
            {
                _context.Sections.Add(section);
                savedSection = section;
            }
            else
            {
                _mapper.Map(section, savedSection);
            }

            await _context.SaveChangesAsync();
            return section;
        }
    }
}