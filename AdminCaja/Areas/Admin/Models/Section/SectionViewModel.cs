﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminCaja.Models;
using Resources;

namespace AdminCaja.Areas.Admin.Models
{
    public class SectionViewModel: SectionListViewModel
    {
        public string Language { get; set; }
        public bool HasContent { get; set; }
        public bool HasDocuments { get; set; }
        public ContentViewModel Content { get; set; }
        public virtual Collection<SubSection> SubSections { get; set; }

    }
}