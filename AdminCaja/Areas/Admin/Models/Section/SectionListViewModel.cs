﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Resources;

namespace AdminCaja.Areas.Admin.Models
{
    public class SectionListViewModel
    {
        public int Id { get; set; }

        [MaxLength(15)]
        [Required(ErrorMessageResourceName ="SeccionNameRequired", ErrorMessageResourceType = typeof(L))]
        [Display(Name = "Name", ResourceType =typeof(L))]
        public string Name { get; set; }

        [Display(Name = "Public", ResourceType = typeof(L))]
        public bool IsPublic { get; set; }
        public bool IsFixed { get; set; }

    }
}