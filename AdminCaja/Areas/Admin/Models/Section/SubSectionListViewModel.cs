﻿using AdminCaja.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminCaja.Areas.Admin.Models.Section
{
    public class SubSectionListViewModel
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string Title { get; set; }
        public string Image { get; set; }
        public string Language { get; set; }
        public SubSectionType Type { get; set; }

    }
}