﻿using AdminCaja.Models;
using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminCaja.Areas.Admin.Models
{
    public class ContentViewModel
    {
        public int Id { get; set; }
        public int SectionId { get; set; }

        [MaxLength(25)]
        [Display(Name = "Title", ResourceType = typeof(L))]
        public string Title { get; set; }

        public string FileName { get; set; }
        public HttpPostedFileBase ImageFile { get; set; }
        public bool ContentEnabled { get; set; }

        [AllowHtml]
        public string Body { get; set; }
        public string Language { get; set; }
        public virtual Collection<File> Files { get; set; }
        public bool HasContent { get; set; }
        public bool HasDocuments { get; set; }

    }
}