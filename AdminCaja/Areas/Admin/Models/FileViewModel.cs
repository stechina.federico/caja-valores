﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminCaja.Areas.Admin.Models
{
    public class FileViewModel
    {
        public int ContentId { get; set; }
        public HttpPostedFileBase file { get; set; }
        public string language { get; set; }
        public string FileName { get; set; }
    }
}