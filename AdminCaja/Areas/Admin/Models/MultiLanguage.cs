﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminCaja.Areas.Admin.Models
{
    public class MultiLanguage<T>
    {
        public T Spanish { get; set; }
        public T English { get; set; }
    }
}