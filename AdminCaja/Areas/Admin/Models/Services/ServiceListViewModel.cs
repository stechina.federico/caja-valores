﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminCaja.Areas.Admin.Models.Services
{
    public class ServiceListViewModel
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Title", ResourceType = typeof(L))]
        [Required]
        [MaxLength(25)]
        public string Title { get; set; }

    }
}