﻿using AdminCaja.Models;
using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminCaja.Areas.Admin.Models.Services
{
    public class ServiceViewModel
    {
        [Key]
        public int Id { get; set; }
        [Display(Name ="CardTitle", ResourceType =typeof(L))]
        [Required(ErrorMessageResourceName ="Required", ErrorMessageResourceType =typeof(L))]
        [MaxLength(25)]
        public string Title { get; set; }

        [Display(Name ="Description", ResourceType =typeof(L))]
        public string Description { get; set; }

        [Display(Name= "HoverText", ResourceType =typeof(L))]
        public string HoverText { get; set; }
        public virtual Collection<Procedure> Procedures { get; set; }
        public virtual Collection<ServiceContent> Contents { get; set; }

    }
}