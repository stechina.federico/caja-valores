﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminCaja.Areas.Admin.Models.Services
{
    public class ServiceContentViewModel
    {
        [Key]
        public int Id { get; set; }

        [Display(Name ="Title", ResourceType =typeof(L))]
        [Required(ErrorMessageResourceName ="Required", ErrorMessageResourceType =typeof(L))]
        public string Title { get; set; }
        [AllowHtml]
        public string Body { get; set; }
        public string Language { get; set; }
        public int ServiceId { get; set; }
        public string ServiceTitle { get; set; }
    }
}