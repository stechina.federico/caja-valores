﻿using AdminCaja.Models;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminCaja.Areas.Admin.Models.Procedures
{
    public class ProcedureFileViewModel
    {
        public int Id { get; set; }
        public int ProcedureId { get; set; }

        [Display(Name ="Title", ResourceType = typeof(L))]
        [Required(ErrorMessageResourceName ="Required", ErrorMessageResourceType =typeof(L))]
        public string Name { get; set; }
        public HttpPostedFileBase file { get; set; }
        public string Path { get; set; }
        public string FileName { get; set; }

        [Range(1,100, ErrorMessageResourceName ="FileOrderInvalid", ErrorMessageResourceType =typeof(L))]
        public int Order { get; set; }
        public ProcedureFileType Type { get; set; }
    }

}