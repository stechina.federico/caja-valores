﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminCaja.Areas.Admin.Models.Procedures
{
    public class ProcedureTypeViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Name", ResourceType = typeof(L))]
        [Required]
        public string Name { get; set; }

        [Required]
        public int CustomerTypeId { get; set; }
        public string CustomerType { get; set; }
        public IEnumerable<SelectListItem> CustomerTypes { get; set; }
    }
}