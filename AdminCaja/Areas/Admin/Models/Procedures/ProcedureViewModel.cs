﻿using AdminCaja.Models;
using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminCaja.Areas.Admin.Models.Procedures
{
    public class ProcedureViewModel
    {
        public int Id { get; set; }

        [Display(Name = "ProcedureType", ResourceType = typeof(L))]
        public int ProcedureTypeId { get; set; }

        [Display(Name = "ProcedureType", ResourceType = typeof(L))]
        public string ProcedureTypeName { get; set; }

        [Display(Name = "CustomerType", ResourceType = typeof(L))]
        public string ProcedureTypeCustomerTypeName { get; set; }

        [Display(Name = "Name", ResourceType = typeof(L))]
        [Required]
        public string Name { get; set; }

        //[Display(Name ="Description", ResourceType =typeof(L))]
        //[Required]
        //public string Description { get; set; }

        [Display(Name = "WhatIsIt", ResourceType = typeof(L))]
        public string WhatIsIt { get; set; }

        [Display(Name = "ForWhom", ResourceType = typeof(L))]
        public string ForWhom { get; set; }

        [Display(Name = "HowTo", ResourceType = typeof(L))]
        public string HowTo { get; set; }

        [Display(Name = "WhereIs", ResourceType = typeof(L))]
        public string WhereIs { get; set; }

        [Display(Name = "Area", ResourceType = typeof(L))]
        public string Area { get; set; }

        public virtual Collection<ProcedureFileViewModel> Files { get; set; }

        //Listas
        public IEnumerable<SelectListItem> ProcedureTypes { get; set; }
        public IEnumerable<SelectListItem> Services { get; set; }
        public int[] SelectedServices { get; set; }


    }
}