﻿using AdminCaja.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminCaja.Areas.Admin.Models.Financial
{
    public class FinancialViewModel
    {
        public FinancialViewModel()
        {
            Year = DateTime.Now.Year;
            Month = DateTime.Now.Month;
        }

        public int Id { get; set; }
        public FinancialStatesTypeEnum Tipo { get; set; }

        [Required]
        public string Title { get; set; }
        public HttpPostedFileBase HeaderImage { get; set; }
        public string HeaderImageName { get; set; }

        [Required]
        public int Month { get; set; }

        [Required]
        public int Year { get; set; }
        public HttpPostedFileBase FinancialData { get; set; }
        public string FinancialDataName { get; set; }
        public HttpPostedFileBase Balance { get; set; }
        public string BalanceName { get; set; }
        public string Language { get; set; }

        public List<SelectListItem> MotnthList
        {
            get
            {
                return DateTimeFormatInfo
               .InvariantInfo
               .MonthNames
               .Select((monthName, index) => new SelectListItem
               {
                   Value = (index + 1).ToString(),
                   Text = monthName
               }).ToList();
            }
        }

        public List<SelectListItem> YearList
        {
            get
            {
                List<SelectListItem> years = new List<SelectListItem>();
                for (int i = DateTime.Now.Year -2; i < DateTime.Now.Year + 10; i++)
                {
                    years.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
                }

                return years;
            }
            
        }
    }
}