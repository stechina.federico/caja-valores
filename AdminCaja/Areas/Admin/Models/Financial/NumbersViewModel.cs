﻿using AdminCaja.Helpers;
using AdminCaja.Models;
using AdminCaja.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminCaja.Areas.Admin.Models.Financial
{
    public class NumbersViewModel
    {
        public int Id { get; set; }
        public FinancialStatesTypeEnum Tipo { get; set; }

        [Required]
        public string Title { get; set; }
        public HttpPostedFileBase HeaderImage { get; set; }
        public string HeaderImageName { get; set; }
        public HttpPostedFileBase FinancialData { get; set; }
        public string FinancialDataName { get; set; }
        public string Language { get; set; }

        [Required]
        public string SelectedPeriod { get; set; }
        public IEnumerable<SelectListItem> QuarterList
        {
            get
            {
                return QuarterHelper.getPeriods()
                    .Select(x=> new SelectListItem {
                        Text = x.Name,
                        Value = x.Year.ToString("0000") + x.Quarter.ToString("00")
                    })
                    .ToList();
            }
        }

    }
}