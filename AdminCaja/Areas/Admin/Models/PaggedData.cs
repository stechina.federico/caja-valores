﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminCaja.Areas.Admin.Models
{

    public class PaggedData
    {
        public int Items { get; set; }
        public int ItemsPerPage { get; set; }
        public int Pages
        {
            get
            {
                return (int)Math.Ceiling((double)Items / (double)ItemsPerPage);
            }
        }
        public int CurrentPage { get; set; }
    }

    public class PaggedData<T>: PaggedData
    {
        public List<T> Data { get; set; }
    }
}