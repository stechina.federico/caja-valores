﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminCaja.Areas.Admin.Models
{
    public class DefinicionLenguajes
    {
        public string Name { get; set; }
        public string lang { get; set; }
    }
}