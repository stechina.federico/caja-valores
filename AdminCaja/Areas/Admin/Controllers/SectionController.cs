﻿using AdminCaja.Areas.Admin.Models;
using AdminCaja.Areas.Admin.Models.Section;
using AdminCaja.Areas.Admin.Services;
using AdminCaja.Controllers;
using AdminCaja.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AdminCaja.Areas.Admin.Controllers
{
    [Authorize]
    public class SectionController : BaseController
    {
        private ISectionService _sectionService;
        private IFileService _fileService;
        private IMapper _mapper;
        private ApplicationDbContext _context;

        public SectionController(ISectionService sectionService, IFileService fileService, IMapper mapper, ApplicationDbContext applicationDbContext)
        {
            _sectionService = sectionService;
            _fileService = fileService;
            _mapper = mapper;
            _context = applicationDbContext;
        }

        #region Secciones
            [HttpGet]
            public async Task<ActionResult> Index(int Page = 1, int ItemsPerPage = 10)
            {
                var items = await _sectionService.GetSectionsAsync(Page, ItemsPerPage);
                return View(items);
            }

            [HttpGet]
            public ActionResult NewSectionListItem()
            {
                var model = new SectionListViewModel();
                return PartialView("NewSection",model);
            }

            [HttpPost]
            public async Task<ActionResult> SaveSectionListItem(SectionListViewModel model)
            {
                if (ModelState.IsValid)
                {
                    var section = _mapper.Map<Section>(model);
                    section = await _sectionService.SaveSectionAsync(section);
                    return Json(new ResultModelModalResult { execute = true, url = Url.Action("EditSection", new { id = section.Id }) });
                }

                return View(model);
            }

            [HttpGet]
            public async Task<ActionResult> EditSection(int id, string lang)
            {
                if(string.IsNullOrEmpty(lang)) lang = "es";

                var section = await _sectionService.GetSectionAsync(id);
                if(section != null)
                {
                    var content = section.Contents.Where(_ => _.Language == lang).FirstOrDefault();
                    if(content == null)
                    {
                        content = new Content { Language = lang, SectionId = section.Id, Section = section };
                    }

                    var model = _mapper.Map<SectionViewModel>(section);
                    model.Content = _mapper.Map<ContentViewModel>(content);
                    model.Language = lang;
                
                    return View(model);
                }

                return HttpNotFound();
            }
            [HttpGet]
            public async Task<ActionResult> GetContent(int Id)
            {
                var content = await _sectionService.GetContentAsync(Id);
                return PartialView("EditForm", _mapper.Map<ContentViewModel>(content));
            }

            [HttpPost]
            public async Task<ActionResult> SaveSection(ContentViewModel model)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        var content = _mapper.Map<Content>(model);

                        if (model.ImageFile != null && model.ImageFile.ContentLength > 0)
                        {
                            if (!string.IsNullOrEmpty(content.Image))
                                _fileService.DeleteImage(content.Image);

                            content.Image = _fileService.SaveFile(model.ImageFile);
                        }

                        content = await _sectionService.SaveContentAsync(content);

                        return PartialView("EditForm", _mapper.Map<ContentViewModel>(content));

                    }catch (Exception ex)
                    {
                        return HttpNotFound();
                    }
                }

                return PartialView("EditForm", model);
            }

            [HttpPost]
            public async Task<ActionResult> AddFile(FileViewModel model)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        if (model.file != null && model.file.ContentLength > 0)
                        {
                            var fileName = _fileService.SaveFile(model.file);

                            var file = new File { ContentId = model.ContentId, Path = fileName, FileName = model.file.FileName, Language = model.language };

                            _context.Files.Add(file);
                            await _context.SaveChangesAsync();

                            return Json(new ResultModelSuccess());
                        }
                    }
                    catch (Exception ex)
                    {
                        return Json(new ResultModel(ex));
                    }
                }

                return Json(new ResultModel(new Exception("Modelo no valido")));
            }


            [HttpGet]
            public async Task<ActionResult> DeleteFile(int Id)
            {
                var file = await _context.Files
                    .Include(x => x.Content)
                    .Where(x => x.Id == Id)
                    .FirstOrDefaultAsync();

                if(file != null)
                {
                    var content = file.Content;
                    _fileService.DeleteImage(file.Path);
                    _context.Files.Remove(file);
                    await _context.SaveChangesAsync();

                    return PartialView("EditForm", _mapper.Map<ContentViewModel>(content));
                }

                return HttpNotFound();
            }
        #endregion

        #region SubSecciones
        public async Task<ActionResult> SubSections(int Id)
        {
            var subSections = await _context.SubSection.Where(x => x.SectionId == Id && x.Language == "es")
                .Include(x=> x.Section)
                .ToListAsync();
            var model = _mapper.Map<List<SubSectionListViewModel>>(subSections);
            return PartialView("_SubSectionsPartial",model);
        }


        #endregion
    }
}