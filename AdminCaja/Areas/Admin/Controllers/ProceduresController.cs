﻿using AdminCaja.Areas.Admin.Models;
using AdminCaja.Areas.Admin.Models.Procedures;
using AdminCaja.Areas.Admin.Services;
using AdminCaja.Models;
using AutoMapper;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AdminCaja.Areas.Admin.Controllers
{
    public class ProceduresController : Controller
    {
        private ApplicationDbContext _context;
        private IMapper _mapper;
        private IFileService _fileService;

        public ProceduresController(ApplicationDbContext context, IMapper mapper, IFileService fileService)
        {
            _context = context;
            _mapper = mapper;
            _fileService = fileService;
        }

        #region Tipos
        public async Task<ActionResult> TypeIndex(int Page = 1, int ItemsPerPage = 10)
        {
            PaggedData<ProcedureTypeViewModel> result = new PaggedData<ProcedureTypeViewModel>();

            result.Items = await _context.ProcedureTypes.CountAsync();
            result.ItemsPerPage = ItemsPerPage;
            result.CurrentPage = Page;

            var sections = await _context.ProcedureTypes
                .Include(x=> x.CustomerType)
                .OrderBy(x => x.Id)
                .Skip(result.ItemsPerPage * (result.CurrentPage - 1))
                .Take(result.ItemsPerPage)
                .ToListAsync();

            result.Data = _mapper.Map<List<ProcedureType>, List<ProcedureTypeViewModel>>(sections);

            return View(result);
        }

        public async Task<ActionResult> AddType()
        {
            ProcedureTypeViewModel model = new ProcedureTypeViewModel();
            model.CustomerTypes = await GetCustomerTypes();

            return View("EditType", model);
        }

        public async Task<ActionResult> EditType(int Id)
        {
            var savedProcedureType = await _context.ProcedureTypes.FirstOrDefaultAsync(x => x.Id == Id);

            ProcedureTypeViewModel model = _mapper.Map<ProcedureTypeViewModel>(savedProcedureType);
            model.CustomerTypes = await GetCustomerTypes();

            return View("EditType", model);
        }


        [HttpPost]
        public async Task<ActionResult> SaveProcedureType(ProcedureTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var savedProcedureType = await _context.ProcedureTypes.FirstOrDefaultAsync(x => x.Id == model.Id);

                if(savedProcedureType == null)
                {
                    savedProcedureType = _mapper.Map<ProcedureType>(model);
                    _context.ProcedureTypes.Add(savedProcedureType);
                }
                else
                {
                    _mapper.Map(model, savedProcedureType);
                }

                await _context.SaveChangesAsync();
                return RedirectToAction("TypeIndex");
            }

            return View("EditType", model);
        }

        public async Task<ActionResult> DeleteType(int Id)
        {
            var procedureType = await _context.ProcedureTypes
                        .FirstOrDefaultAsync(x => x.Id == Id);

            var vm = _mapper.Map<ProcedureTypeViewModel>(procedureType);
            return PartialView("DeleteProcedureType",vm);
        }

        [HttpPost]
        public async Task<ActionResult> ConfirmDeleteType(int Id)
        {
            var ProcedureTypeContent = await _context.ProcedureTypes
            .FirstOrDefaultAsync(x => x.Id == Id);

            if (ProcedureTypeContent != null)
            {
                _context.ProcedureTypes.Remove(ProcedureTypeContent);
                await _context.SaveChangesAsync();
            }

            return Json(new ResultModelModalResult { execute = true });
        }

        private async Task<IEnumerable<SelectListItem>> GetCustomerTypes()
        {
            return await _context.CustomerTypes.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToListAsync();
        }
        #endregion

        #region Procedures
        // GET: Admin/Procedures
        public async Task<ActionResult> Index(int Page = 1, int ItemsPerPage = 10)
        {
            PaggedData<ProcedureViewModel> result = new PaggedData<ProcedureViewModel>();

            result.Items = await _context.Procedures.CountAsync();
            result.ItemsPerPage = ItemsPerPage;
            result.CurrentPage = Page;

            var procedures = await _context.Procedures
                .Include(x=> x.ProcedureType)
                .Include(x=> x.ProcedureType.CustomerType)
                .OrderBy(x => x.Id)
                .Skip(result.ItemsPerPage * (result.CurrentPage - 1))
                .Take(result.ItemsPerPage)
                .ToListAsync();

            result.Data = _mapper.Map<List<Procedure>, List<ProcedureViewModel>>(procedures);


            return View(result);
        }

        public async Task<ActionResult> Add()
        {
            ProcedureViewModel model = new ProcedureViewModel();
            model.ProcedureTypes = await GetProcedureTypes();
            model.Services = await GetServices();

            return View("Edit", model);
        }

        public async Task<ActionResult> Edit(int Id)
        {
            var savedProcedure = await _context.Procedures
                .Include(x=> x.Services)
                .FirstOrDefaultAsync(x => x.Id == Id);

            ProcedureViewModel model = _mapper.Map<ProcedureViewModel>(savedProcedure);
            model.ProcedureTypes = await GetProcedureTypes();
            model.Services = await GetServices();
            model.SelectedServices = savedProcedure.Services.Select(x => x.Id).ToArray();

            return View("Edit", model);
        }


        [HttpPost]
        public async Task<ActionResult> Save(ProcedureViewModel model)
        {
            if (ModelState.IsValid)
            {
                var savedProcedure = await _context.Procedures
                    .Include(x=> x.Services)
                    .FirstOrDefaultAsync(x => x.Id == model.Id);

                if (savedProcedure == null)
                {
                    savedProcedure = _mapper.Map<Procedure>(model);

                    if (model.SelectedServices != null)
                    {
                        var services = await _context.Services.Where(x => model.SelectedServices.Contains(x.Id)).ToListAsync();
                        foreach (var item in services)
                        {
                            item.Procedures.Add(savedProcedure);
                        }
                    }
                    _context.Procedures.Add(savedProcedure);
                }
                else
                {
                    _mapper.Map(model, savedProcedure);

                    if (model.SelectedServices != null)
                    { 
                        var eliminados = savedProcedure.Services.Where(x => !model.SelectedServices.Contains(x.Id)).ToList();
                        eliminados.ForEach(x => x.Procedures.Remove(savedProcedure));
                    
                        var services = await _context.Services.Where(x => model.SelectedServices.Contains(x.Id)).ToListAsync();
                        foreach (var item in services)
                        {
                            item.Procedures.Add(savedProcedure);
                        }
                    }
                    else
                    {
                        savedProcedure.Services.ForEach(x => x.Procedures.Remove(savedProcedure));
                    }

                }

                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            model.ProcedureTypes = await GetProcedureTypes();
            model.Services = await GetServices();

            return View("Edit", model);
        }

        private async Task<IEnumerable<SelectListItem>> GetProcedureTypes()
        {
            return await _context.ProcedureTypes.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToListAsync();
        }

        private async Task<IEnumerable<SelectListItem>> GetServices()
        {
            return await _context.Services.Select(x => new SelectListItem { Text = x.Title, Value = x.Id.ToString() }).ToListAsync();
        }

        #endregion

        #region Files
        [HttpGet]
        public async Task<ActionResult> NewFile(int procedureId, ProcedureFileType type)
        {
            ProcedureFileViewModel model = new ProcedureFileViewModel
            {
                ProcedureId = procedureId,
                Type = type,
                Order = await GetMaxOrder(procedureId, type)
            };

            return PartialView("EditFile", model);
        }

        [HttpGet]
        public async Task<ActionResult> EditFile(int Id)
        {
            var file = await _context.ProcedureFiles
            .Include(x => x.Procedure)
            .Where(x => x.Id == Id)
            .FirstOrDefaultAsync();

            var model = _mapper.Map<ProcedureFileViewModel>(file);

            return PartialView("EditFile", model);
        }

        public async Task<ActionResult> AddorUpdateFile(ProcedureFileViewModel model)
        {
            if (ModelState.IsValid)
            {
                var file = await _context.ProcedureFiles.FirstOrDefaultAsync(x => x.Id == model.Id);

                if (file == null)
                {
                    if (model.file != null && model.file.ContentLength > 0)
                    {
                        return await AddFile(model);
                    }
                    else
                    {
                        ModelState.AddModelError("file", "Debe seleccionar un archivo para agregar");
                    }

                }
                else
                {
                    if(file.Order != model.Order)
                    {
                        //reordena 
                        file.Procedure.Files
                            .Where(x => x.Type == file.Type && x.Order > file.Order)
                            .ForEach(x => x.Order--);
                        file.Procedure.Files
                            .Where(x => x.Type == file.Type && x.Order > model.Order)
                            .ForEach(x => x.Order++);
                        file.Order = model.Order;

                        var maxOrder = await GetMaxOrder(model.ProcedureId, model.Type);
                        if (file.Order > maxOrder) file.Order = maxOrder;

                    }

                    if (model.file != null && model.file.ContentLength > 0)
                    {
                        var fileName = _fileService.SaveFile(model.file);
                        _fileService.DeleteImage(file.Path);
                        file.Path = fileName;
                        file.FileName = model.file.FileName;
                    }

                    file.Name = model.Name;
                    await _context.SaveChangesAsync();

                    return Json(new CustomResult(model.ProcedureId, model.Type));
                }
            }

            return PartialView("EditFile", model);

        }
        [HttpPost]
        public async Task<ActionResult> AddFile(ProcedureFileViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (model.file != null && model.file.ContentLength > 0)
                    {
                        var procedure = await _context.Procedures.Include(x => x.Files).FirstOrDefaultAsync(x => x.Id == model.ProcedureId);

                        if(procedure!= null)
                        {
                            var maxOrder = await GetMaxOrder(model.ProcedureId, model.Type);
                            if (procedure.Files.Any(x=> x.Type == model.Type && x.Order == model.Order))
                            {
                                procedure.Files
                                    .Where(x => x.Type == model.Type && x.Order >= model.Order)
                                    .ForEach(x => x.Order++);
                            }
                        
                            var fileName = _fileService.SaveFile(model.file);
                            var file = _mapper.Map<ProcedureFile>(model);
                            file.Path = fileName;
                            file.FileName = model.file.FileName;

                            if (file.Order > maxOrder) file.Order = maxOrder;

                           procedure.Files.Add(file);

                           await _context.SaveChangesAsync();

                           return Json(new CustomResult(model.ProcedureId, model.Type));
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("file", "Debe seleccionar un archivo para agregar");
                    }
                }
                catch (Exception ex)
                {
                    return Json(new ResultModel(ex));
                }
            }

            return PartialView("EditFile", model);
        }

        [HttpGet]
        public async Task<ActionResult> DeleteFile(int Id)
        {
            var file = await _context.ProcedureFiles
                .Include(x => x.Procedure)
                .Where(x => x.Id == Id)
                .FirstOrDefaultAsync();

            var model = _mapper.Map<ProcedureFileViewModel>(file);

            return PartialView("DeleteProcedureFile", model);
        }

        [HttpPost]
        public async Task<ActionResult> ConfirmDeleteFile(ProcedureFileViewModel model)
        {
            var file = await _context.ProcedureFiles
                .Include(x=> x.Procedure)
                .Where(x => x.Id == model.Id)
                .FirstOrDefaultAsync();

            if (file != null)
            {
                
                _fileService.DeleteImage(file.Path);

                file.Procedure.Files
                    .Where(x => x.Type == file.Type && x.Order > file.Order)
                    .ForEach(x => x.Order--);

                _context.ProcedureFiles.Remove(file);

                await _context.SaveChangesAsync();

                return Json(new CustomResult(model.ProcedureId, model.Type));
            }

            return HttpNotFound();
        }

        [HttpGet]
        public async Task<ActionResult> ListFiles(int Id, ProcedureFileType type)
        {
            var fileList = await _context.ProcedureFiles
                .Where(x => x.Type == type && x.ProcedureId == Id)
                .OrderBy(x=> x.Order)
                .ToListAsync();
            var model = _mapper.Map<List<ProcedureFile>, List<ProcedureFileViewModel>>(fileList);

            return PartialView("_documentList", model);
        }
        private async Task<int> GetMaxOrder(int procedureId, ProcedureFileType type)
        {
            int? maxOrden = await _context.ProcedureFiles.Where(x => x.ProcedureId == procedureId && x.Type == type).Select(x => (int?)x.Order).MaxAsync();

            if (!maxOrden.HasValue) maxOrden = 0;
            maxOrden++;

            return maxOrden.Value;
        }
        #endregion
    }

    public class CustomResult : ResultModelModalResult
    {
        public CustomResult(int ProcedureId, ProcedureFileType type): base()
        {
            execute = true;
            url = $"/Admin/Procedures/ListFiles/{ProcedureId}?type={type}";
            this.type = type;
        }

        public ProcedureFileType type { get; set; }
    }
}