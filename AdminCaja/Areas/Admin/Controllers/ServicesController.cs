﻿using AdminCaja.Areas.Admin.Models;
using AdminCaja.Areas.Admin.Models.Services;
using AdminCaja.Controllers;
using AdminCaja.Helpers;
using AdminCaja.Models;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AdminCaja.Areas.Admin.Controllers
{
    public class ServicesController : BaseController
    {
        private ApplicationDbContext _context;
        private IMapper _mapper;

        public ServicesController(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

 #region Services
        // GET: Admin/Services
        public async Task<ActionResult> Index(int itemsPerPage = 10, int Page = 1)
        {
            PaggedData<ServiceListViewModel> result = new PaggedData<ServiceListViewModel>();

            result.Items = await _context.Services.CountAsync();
            result.ItemsPerPage = itemsPerPage;
            result.CurrentPage = Page;

            var services = await _context.Services
                .OrderBy(x => x.Id)
                .Skip(result.ItemsPerPage * (result.CurrentPage - 1))
                .Take(result.ItemsPerPage)
                .ToListAsync();

            result.Data = _mapper.Map<List<Service>, List<ServiceListViewModel>>(services);

            return View(result);
        }

        public ActionResult NewService()
        {
            var vm = new ServiceViewModel();
            return View("FormService", vm);
        }

        public async Task<ActionResult> EditService(int Id)
        {
            var service = await _context.Services.Include(x => x.Contents).FirstOrDefaultAsync(x => x.Id == Id);
            return View("FormService", _mapper.Map<ServiceViewModel>(service));
        }

        [HttpPost]
        public async Task<ActionResult> SaveService(ServiceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var savedService = await _context.Services
                    .Include(x => x.Contents)
                    .FirstOrDefaultAsync(x => x.Id == model.Id);

                if(savedService == null)
                {
                    savedService = _mapper.Map<Service>(model);
                    _context.Services.Add(savedService);
                }
                else
                {
                    _mapper.Map(model, savedService);
                }

                await _context.SaveChangesAsync();

                model = _mapper.Map<ServiceViewModel>(savedService);
            }

            return View("FormService", model);
        }

        public async Task<ActionResult> DeleteService(int Id)
        {
            var service = await _context.Services
                        .FirstOrDefaultAsync(x => x.Id == Id);

            var vm = _mapper.Map<ServiceListViewModel>(service);
            return PartialView(vm);
        }

        [HttpPost]
        public async Task<ActionResult> ConfirmDeleteService(int Id)
        {
            var service = await _context.Services
            .FirstOrDefaultAsync(x => x.Id == Id);

            if(service != null)
            {
                _context.Services.Remove(service);
                await _context.SaveChangesAsync();
            }

            return Json(new ResultModelModalResult { execute = true});
        }
        #endregion
        
        #region ServiceContent
        public async Task<ActionResult> EditServiceContent(int Id = -1, string lang = "es", int ServiceId = -1)
        {
            ServiceContentViewModel vm;
            var serviceContent = await _context.ServiceContents
                .Include(x => x.Service)
                .FirstOrDefaultAsync(x => x.Id == Id);

            if(serviceContent == null)
            {
                var service = await _context.Services.FirstOrDefaultAsync(x => x.Id == ServiceId);

                if (service == null)
                    return BadRequest();

                vm = new ServiceContentViewModel() {Language = lang };
                vm.ServiceTitle = service.Title;
                vm.ServiceId = service.Id;
                vm.Language = lang;
            }
            else
            {
                vm = _mapper.Map<ServiceContentViewModel>(serviceContent);
                vm.ServiceTitle = serviceContent.Service?.Title;
            }
            
            return View("FormServiceContent", vm);
        }

        public async Task<ActionResult> SaveServiceContent(ServiceContentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var savedServiceContent = await _context.ServiceContents
                    .Include(x => x.Service)
                    .FirstOrDefaultAsync(x => x.Id == model.Id);

                if (savedServiceContent == null)
                {
                    var service = await _context.Services.FirstOrDefaultAsync(x => x.Id == model.ServiceId);

                    if (service == null)
                        return BadRequest();

                    savedServiceContent = _mapper.Map<ServiceContent>(model);
                    savedServiceContent.Service = service;
                    _context.ServiceContents.Add(savedServiceContent);
                }
                else
                {
                    _mapper.Map(model, savedServiceContent);
                }

                await _context.SaveChangesAsync();

                model = _mapper.Map<ServiceContentViewModel>(savedServiceContent);

                return RedirectToAction("EditService", new { Id = model.ServiceId });
            }

            return View("FormServiceContent", model);
        }

        public async Task<ActionResult> DeleteServiceContent(int Id)
        {
            var service = await _context.ServiceContents
                        .FirstOrDefaultAsync(x => x.Id == Id);

            var vm = _mapper.Map<ServiceContentListViewModel>(service);
            return PartialView(vm);
        }

        [HttpPost]
        public async Task<ActionResult> ConfirmDeleteServiceContent(int Id)
        {
            int ServiceId = 0;
            var serviceContent = await _context.ServiceContents
            .FirstOrDefaultAsync(x => x.Id == Id);

            if (serviceContent != null)
            {
                ServiceId = serviceContent.ServiceId;
                _context.ServiceContents.Remove(serviceContent);
                await _context.SaveChangesAsync();
            }

            return Json(new ResultModelModalResult { url = $"/Admin/Services/GetContentList/{ServiceId}" });
        }

        public async Task<ActionResult> GetContentList(int Id)
        {
            var service = await _context.Services.Include(x => x.Contents).FirstOrDefaultAsync(x => x.Id == Id);
            return PartialView("FormServiceListContent", _mapper.Map<ServiceViewModel>(service));
        }

        #endregion

    }
}