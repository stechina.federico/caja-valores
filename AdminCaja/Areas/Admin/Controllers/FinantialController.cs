﻿using AdminCaja.Areas.Admin.Models.Financial;
using AdminCaja.Areas.Admin.Services;
using AdminCaja.Models;
using AutoMapper;
using NPOI.SS.UserModel;
using NPOI.XSSF.Extractor;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AdminCaja.Areas.Admin.Controllers
{
    public class FinantialController : Controller
    {
        private ApplicationDbContext _context;
        private IMapper _mapper;
        private IFileService _fileService;

        public FinantialController(ApplicationDbContext context, IMapper mapper, IFileService fileService)
        {
            _context = context;
            _mapper = mapper;
            _fileService = fileService;
        }

        // GET: Admin/Finantial
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EditFinantial(string lang = "es")
        {
            FinancialStatesModel financial = _context.FinancialStatesModels.FirstOrDefault(x => x.Language == lang && x.Tipo == FinancialStatesTypeEnum.Financial);
            var model = new FinancialViewModel { Language = lang, Tipo = FinancialStatesTypeEnum.Financial };

            if (financial != null)
            {
                model.Title = financial.Title;
            }

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> SaveFinantial(FinancialViewModel model)
        {
            if (ModelState.IsValid)
            {
                await ImportExcelFinancial(model.FinancialData, model.Year, model.Month);

                FinancialStatesModel financial = await _context.FinancialStatesModels.FirstOrDefaultAsync(x => x.Language == model.Language && x.Tipo == FinancialStatesTypeEnum.Financial);

                if(financial == null)
                {
                    financial = new FinancialStatesModel {Language = model.Language, Tipo = FinancialStatesTypeEnum.Financial };
                    _context.FinancialStatesModels.Add(financial);
                }

                if (model.HeaderImage != null && model.HeaderImage.ContentLength > 0)
                {
                    if (!string.IsNullOrEmpty(financial.HeaderImage))
                        _fileService.DeleteImage(financial.HeaderImage);

                    financial.HeaderImage = _fileService.SaveFile(model.HeaderImage);
                }

                if (model.Balance != null && model.Balance.ContentLength > 0)
                {
                    if(financial.Balances != null)
                    {
                        var balanceExistente = financial.Balances.FirstOrDefault(x => x.Year == model.Year && x.Language == model.Language);
                        if(balanceExistente != null)
                        {
                            _fileService.DeleteImage(balanceExistente.Path);
                            financial.Balances.Remove(balanceExistente);
                        }
                    }

                    var balance = new FinancialBalance
                    {
                        Language = model.Language,
                        Year = model.Year,
                        Path = _fileService.SaveFile(model.HeaderImage),
                        FileName = model.Balance.FileName
                    };

                    financial.Balances.Add(balance);
                }

                financial.Title = model.Title;

                await _context.SaveChangesAsync();
            }
            return View("EditFinantial", model);
        }

        public ActionResult EditNumbers(string lang = "es")
        {
            FinancialStatesModel financial = _context.FinancialStatesModels.FirstOrDefault(x => x.Language == lang && x.Tipo == FinancialStatesTypeEnum.Numbers) ;
            var model = new NumbersViewModel { Language = lang, Tipo = FinancialStatesTypeEnum.Numbers };

            if (financial != null)
            {
                model.Title = financial.Title;
            }

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> SaveNumbers(NumbersViewModel model)
        {
            if (model.SelectedPeriod.Length != 6)
            {
                ModelState.AddModelError("SelectedPeriod", "Debe seleccionar un período válido");
            }

            if (ModelState.IsValid)
            {

                int year = int.Parse(model.SelectedPeriod.Substring(0, 4));
                int quarter = int.Parse(model.SelectedPeriod.Substring(4, 2));

                await ImportExcelNumbers(model.FinancialData, year, quarter);

                FinancialStatesModel financial = await _context.FinancialStatesModels.FirstOrDefaultAsync(x => x.Language == model.Language && x.Tipo == FinancialStatesTypeEnum.Numbers);

                if (financial == null)
                {
                    financial = new FinancialStatesModel { Language = model.Language, Tipo = FinancialStatesTypeEnum.Numbers };
                    _context.FinancialStatesModels.Add(financial);
                }

                if (model.HeaderImage != null && model.HeaderImage.ContentLength > 0)
                {
                    if (!string.IsNullOrEmpty(financial.HeaderImage))
                        _fileService.DeleteImage(financial.HeaderImage);

                    financial.HeaderImage = _fileService.SaveFile(model.HeaderImage);
                }

                financial.Title = model.Title;

                await _context.SaveChangesAsync();
            }

            return View("EditNumbers", model);
        }

        #region Importador

        private async Task ImportExcelFinancial(HttpPostedFileBase financialData, int year, int month)
        {
            if(financialData != null && financialData.ContentLength > 0)
            {
                try
                {
                    XSSFWorkbook xls = new XSSFWorkbook(financialData.InputStream);

                    for (int i = 0; i < xls.NumberOfSheets; i++)
                    {
                        XSSFSheet sheet = xls.GetSheetAt(i) as XSSFSheet;
                        switch (sheet.SheetName)
                        {
                            case "EstadosFinancieros":
                                var financialStates = await ImportEstadosFinancieros(sheet, year);
                                break;

                            default:
                                break;
                        }
                    }
                }
                catch(Exception ex)
                {

                }
            }
        }

        private async Task ImportExcelNumbers(HttpPostedFileBase financialData, int year, int quarter)
        {
            if (financialData != null && financialData.ContentLength > 0)
            {
                try
                {
                    XSSFWorkbook xls = new XSSFWorkbook(financialData.InputStream);

                    for (int i = 0; i < xls.NumberOfSheets; i++)
                    {
                        XSSFSheet sheet = xls.GetSheetAt(i) as XSSFSheet;
                        switch (sheet.SheetName)
                        {
                            case "DatosRelevantes":
                                var datosRelevantes = await ImportarDatosRelevantes(sheet, year, quarter);
                                break;

                            case "ACRyP":
                                var acryp = await ImportarACRyP(sheet, year, quarter);
                                break;
                            case "ADC":
                                var adc = await ImportarAdc(sheet, year, quarter);
                                break;
                            case "ActivosEnCustodia":
                                var activosEnCustodia = await ImportarActivosEnCustodia(sheet, year, quarter);
                                break;
                            case "DepositoInternacional":
                                var deopositoInternacional = await ImportarDepositoInternacional(sheet, year, quarter);
                                break;
                            case "OtrosServicios":
                                var otros = await ImportarOtrosServicios(sheet, year, quarter);
                                break;
                            default:
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        private async Task<OtrosServicios> ImportarOtrosServicios(XSSFSheet sheet, int year, int quarter)
        {
            OtrosServicios data = await _context.OtrosServicios.FirstOrDefaultAsync(x => x.Year == year && x.Quarter == quarter);

            if(data == null) {     
                data = new OtrosServicios {
                    Year = year,
                    Quarter = quarter,
                    CPD = new OtrosServiciosData
                    {
                        CasaCentralPesos = new OtrosServiciosDataDetail(),
                        CasaCentralUs = new OtrosServiciosDataDetail(),
                        SucursalesPesos = new OtrosServiciosDataDetail(),
                        SucursalesUs = new OtrosServiciosDataDetail(),
                    }
,
                    PagareBursatil = new OtrosServiciosData
                    {
                        CasaCentralPesos = new OtrosServiciosDataDetail(),
                        CasaCentralUs = new OtrosServiciosDataDetail(),
                        SucursalesPesos = new OtrosServiciosDataDetail(),
                        SucursalesUs = new OtrosServiciosDataDetail(),
                    }
                };

                _context.OtrosServicios.Add(data);
            }

            IRow row;
            row = sheet.GetRow(1);

            if (row != null)
            {
                data.Letters = GetDecimalValue(row.GetCell(7));
            }

            row = sheet.GetRow(3);

            if (row != null)
            {
                data.CPD.CasaCentralPesos.Quantity = GetIntValue(row.GetCell(0));
                data.CPD.CasaCentralPesos.Amount = GetDecimalValue(row.GetCell(1));
                data.PagareBursatil.CasaCentralPesos.Quantity = GetIntValue(row.GetCell(3));
                data.PagareBursatil.CasaCentralPesos.Amount = GetDecimalValue(row.GetCell(4));
            }

            row = sheet.GetRow(4);

            if (row != null)
            {
                data.CPD.CasaCentralUs.Quantity = GetIntValue(row.GetCell(0));
                data.CPD.CasaCentralUs.Amount = GetDecimalValue(row.GetCell(1));
                data.PagareBursatil.CasaCentralUs.Quantity = GetIntValue(row.GetCell(3));
                data.PagareBursatil.CasaCentralUs.Amount = GetDecimalValue(row.GetCell(4));
            }

            row = sheet.GetRow(7);

            if (row != null)
            {
                data.CPD.SucursalesPesos.Quantity = GetIntValue(row.GetCell(0));
                data.CPD.SucursalesPesos.Amount = GetDecimalValue(row.GetCell(1));
                data.PagareBursatil.SucursalesPesos.Quantity = GetIntValue(row.GetCell(3));
                data.PagareBursatil.SucursalesPesos.Amount = GetDecimalValue(row.GetCell(4));
            }

            row = sheet.GetRow(8);

            if (row != null)
            {
                data.CPD.SucursalesUs.Quantity = GetIntValue(row.GetCell(0));
                data.CPD.SucursalesUs.Amount = GetDecimalValue(row.GetCell(1));
                data.PagareBursatil.SucursalesUs.Quantity = GetIntValue(row.GetCell(3));
                data.PagareBursatil.SucursalesUs.Amount = GetDecimalValue(row.GetCell(4));
            }

            return data;
        }

        private async Task<DepositoInternacional> ImportarDepositoInternacional(XSSFSheet sheet, int year, int month)
        {
            DepositoInternacional data = await _context.DepositoInternacional.FirstOrDefaultAsync(x => x.Year == year && x.Month == month);

            if(data == null) { 
                data = new DepositoInternacional { Year = year, Month = month };
                _context.DepositoInternacional.Add(data);
            }

            IRow row;
            row = sheet.GetRow(1);

            if (row != null)
            {
                data.Euroclear = GetDecimalValue(row.GetCell(0));
                data.ClearStream = GetDecimalValue(row.GetCell(1));
                data.DTC = GetDecimalValue(row.GetCell(2));
                data.CBLC = GetDecimalValue(row.GetCell(3));
                data.Iberclear = GetDecimalValue(row.GetCell(4));
            }

            return data;
        }

        private async Task<ActivosEnCustodia> ImportarActivosEnCustodia(XSSFSheet sheet, int year, int quarter)
        {
            ActivosEnCustodia activosEnCustodia = await _context.ActivosEnCustodia.FirstOrDefaultAsync(x => x.Year == year && x.Quarter == quarter);

            if(activosEnCustodia == null)
            {
                activosEnCustodia = new ActivosEnCustodia {
                    Year = year,
                    Quarter = quarter,
                    Totals = new TotalesAEC { Graph = new List<FinancialStatesGraph>()},
                    Details = new List<DetailedAEC>()
                };
                _context.ActivosEnCustodia.Add(activosEnCustodia);
            }

            IRow row;
            row = sheet.GetRow(0);
            if (row != null)
            {
                activosEnCustodia.Totals.TotalVPN = GetDecimalValue(row.GetCell(2));
            }

            //Totlales Columnas A y B
            activosEnCustodia.Totals.Graph.Clear();
            int i = 1;
            do
            {
                row = sheet.GetRow(i);
                if (row == null || string.IsNullOrEmpty(row.GetCell(0).StringCellValue))
                    break;

                activosEnCustodia.Totals.Graph.Add(new FinancialStatesGraph
                {
                    Name = row.GetCell(0).StringCellValue,
                    Value = GetDecimalValue(row.GetCell(1))
                });

                i++;

            } while (i < sheet.PhysicalNumberOfRows);

            // k Columna
            // j Fila
            activosEnCustodia.Details.Clear();
            for (int j = 1; j <= 8; j++)
            {
                row = sheet.GetRow(j);

                var detail = new DetailedAEC
                {
                    ColName = row.GetCell(3).StringCellValue,
                    Graph = new List<FinancialStatesGraph>()
                };

                for (int k=4; k <= 7; k++)
                {
                    
                    //if (row == null || string.IsNullOrEmpty(row.GetCell(k).StringCellValue))
                    //    break;

                    detail.Graph.Add(new FinancialStatesGraph
                    {
                        Name = sheet.GetRow(0).GetCell(k).StringCellValue,
                        Value = GetDecimalValue(row.GetCell(k))
                    });
                } 

                activosEnCustodia.Details.Add(detail);

            }

            return activosEnCustodia;
        }

        private async Task<ADCData> ImportarAdc(XSSFSheet sheet, int year, int quarter)
        {
            ADCData data = await _context.ADCData.FirstOrDefaultAsync(x => x.Year == year && x.Quarter == quarter);

            if(data == null) { 
                data = new ADCData { Year = year, Quarter = quarter };
                _context.ADCData.Add(data);
            }

            IRow row;
            row = sheet.GetRow(1);

            if (row != null)
            {
                data.SafeVN = GetIntValue(row.GetCell(0));
                data.SaveAmount = GetIntValue(row.GetCell(1));
                data.SafeIntl = GetIntValue(row.GetCell(2));
            }

            row = sheet.GetRow(6);

            if (row != null)
            {
                data.Kind7Amount = GetIntValue(row.GetCell(0));
                data.Kind10Amount = GetIntValue(row.GetCell(1));
                data.Kind7Amount = GetIntValue(row.GetCell(2));
                data.Kind6Amount = GetIntValue(row.GetCell(3));
            }

            row = sheet.GetRow(8);

            if (row != null)
            {
                data.Kind7 = GetDecimalValue(row.GetCell(0));
                data.Kind10 = GetDecimalValue(row.GetCell(1));
                data.Kind7 = GetDecimalValue(row.GetCell(2));
                data.Kind6 = GetDecimalValue(row.GetCell(3));
            }

            return data;
        }

        private async Task<ACRyPData> ImportarACRyP(XSSFSheet sheet, int year, int quarter)
        {
            ACRyPData data = await _context.ACRyPData.FirstOrDefaultAsync(x => x.Year == year && x.Quarter == quarter);

            if(data == null)
            {
                data = new ACRyPData { Year = year, Quarter = quarter };
                _context.ACRyPData.Add(data);
            }

            IRow row;
            row = sheet.GetRow(1);

            if (row != null)
            {
                data.CreditsAmount = GetIntValue(row.GetCell(0));
                data.CreditsPesos = GetDecimalValue(row.GetCell(1));
                data.JudicialOffices = GetIntValue(row.GetCell(2));
                data.RegisteredAccounts = GetIntValue(row.GetCell(3));
                data.People = GetIntValue(row.GetCell(4));
            }

            return data;
        }

        private async Task<RelevantData> ImportarDatosRelevantes(XSSFSheet sheet, int year, int quarter)
        {
            RelevantData relevantData = await _context.RelevantData.FirstOrDefaultAsync(x => x.Year == year && x.Quarter == quarter);

            if(relevantData == null)
            {
                relevantData = new RelevantData { Year = year, Quarter = quarter, GraphData = new List<RelevantDataGraph>() };
                _context.RelevantData.Add(relevantData);
            }
            
            IRow row;
            row = sheet.GetRow(1);

            if (row != null)
            {
                relevantData.SubAccounts = GetIntValue(row.GetCell(0));
            }

            relevantData.GraphData.Clear();
            int i = 5;
            do
            {
                row = sheet.GetRow(i);
                if (row == null || string.IsNullOrEmpty(row.GetCell(0).StringCellValue))
                    break;

                relevantData.GraphData.Add(new RelevantDataGraph
                {
                    Name = row.GetCell(0).StringCellValue,
                    Percent = GetDecimalValue(row.GetCell(1)),
                    Value = GetDecimalValue(row.GetCell(2))
                });

                i++;

            } while (i < sheet.PhysicalNumberOfRows);


            return relevantData;
        }

        private async Task<FinancialStates> ImportEstadosFinancieros(XSSFSheet sheet, int year)
        {
            FinancialStates financialStates = await _context.FinancialStates.FirstOrDefaultAsync(x => x.Year == year);

            if(financialStates == null)
            {
                financialStates = new FinancialStates { Year = year, GraphData = new List<FinancialStatesGraph>() };
                _context.FinancialStates.Add(financialStates);
            }
                
            IRow row;
            row = sheet.GetRow(1);

            if(row != null)
            {
                financialStates.SocialCapital = GetDecimalValue(row.GetCell(0));
                financialStates.NetWorth = GetDecimalValue(row.GetCell(1));
            }

            financialStates.GraphData.Clear();

            int i = 4;
            do
            {
                row = sheet.GetRow(i);
                if (row == null || string.IsNullOrEmpty(row.GetCell(0).StringCellValue))
                    break;
                financialStates.GraphData.Add(new FinancialStatesGraph
                {
                    Name = row.GetCell(0).StringCellValue,
                    Value = GetDecimalValue(row.GetCell(1))
                });
                i++;

            }while(i <= sheet.PhysicalNumberOfRows);

            await _context.SaveChangesAsync();

            return financialStates;
        }

        private decimal GetDecimalValue(ICell cell)
        {
            if(cell.CellType == CellType.Numeric)
            {
                return (decimal)cell.NumericCellValue;
            }if(cell.CellType == CellType.String)
            {
                return Decimal.Parse(cell.StringCellValue);
            }
            else
            {
                return 0;
            }
        }

        private int GetIntValue(ICell cell)
        {
            if (cell.CellType == CellType.Numeric)
            {
                return Convert.ToInt32(cell.NumericCellValue);
            }
            if (cell.CellType == CellType.String)
            {
                return int.Parse(cell.StringCellValue);
            }
            else
            {
                return 0;
            }
        }
        #endregion

        public ActionResult Template()
        {
            return File("~/content/plantilla.xlsx", "application/xls", "Plantilla.xlsx");
        }
    }
}