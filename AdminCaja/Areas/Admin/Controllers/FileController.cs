﻿using AdminCaja.Areas.Admin.Models;
using AdminCaja.Areas.Admin.Services;
using AdminCaja.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminCaja.Areas.Admin.Controllers
{
    public class FileController : BaseController
    {
        private IFileService FileService;

        public FileController(IFileService fileService)
        {
            FileService = fileService;
        }


        [HttpPost]
        public JsonResult UploadFileToContent(FileViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {

                
                if (model.file.ContentLength > 0)
                {
                    return Json(FileService.SaveFile(model.file));
                }
                }
            }
            catch
            {
                ViewBag.Message = "File upload failed!!";
            }

            return Json(false);

        }
    }
}