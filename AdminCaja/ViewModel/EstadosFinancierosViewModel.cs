﻿using AdminCaja.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminCaja.ViewModel
{
    public class EstadosFinancierosViewModel
    {
        public FinancialStatesModel PageData { get; set; }
        public FinancialStates FinancialStates { get; set; }
    }
}