﻿using AdminCaja.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminCaja.ViewModel
{
    public class DatosRelevantesViewModel
    {
        public RelevantData RelevantData { get; set; }
        public List<BarGraphData> Evolucion { get; set; }
    }

    public class BarGraphData
    {
        public string trimestre { get; set; }
        public decimal cantidad { get; set; }
        public string color { get; set; }
    }
}