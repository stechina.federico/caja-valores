﻿using AdminCaja.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminCaja.ViewModel
{
    public class ADCViewModel
    {
        public List<BarGraphData> CustodiaVN { get; set; }
        public List<BarGraphData> CustodiaValorizada { get; set; }
        public List<BarGraphData> CustodiaInternacional { get; set; }
        public ADCData ADCData { get; set; }
    }
}