﻿using AdminCaja.Helpers;
using AdminCaja.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminCaja.ViewModel
{
    public interface ICifrasViewModel
    {
        List<MenuViewModel> Menu { get; set; }
        string MenuActivo { get; set; }
        List<Period> Periodos { get; set; }
        Period PeriodoActual { get; set; }
        FinancialStatesModel PageData { get; set; }

    }
    public class CifrasViewModel<T>: ICifrasViewModel
    {
        public List<MenuViewModel> Menu { get; set; }
        public string MenuActivo { get; set; }
        public FinancialStatesModel PageData { get; set; }
        public T Data { get; set; }
        public List<Period> Periodos { get; set; }
        public Period PeriodoActual { get; set; }
    }

    public class MenuViewModel
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public string Link { get; set; }
    }

}