﻿using AdminCaja.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminCaja.ViewModel
{
    public class TramitesViewModel
    {
        public int SelectedTipoCliente { get; set; }
        public int SelectedTipoTramite { get; set; }
        public int SelectedTramite { get; set; }

        public IEnumerable<SelectListItem> TiposCliente { get; set; }
        public IEnumerable<SelectListItem> TiposTramite { get; set; }
        public IEnumerable<Procedure> Tramites { get; set; }
    }
}