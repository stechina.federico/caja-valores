﻿using AdminCaja.Helpers;
using AdminCaja.Models;
using AdminCaja.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AdminCaja.Controllers
{
    public class InfoFinController : Controller
    {
        private ApplicationDbContext _context;

        public InfoFinController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: InfoFin
        public ActionResult Estados()
        {
            var model = new EstadosFinancierosViewModel
            {
                 PageData = _context.FinancialStatesModels.FirstOrDefault(x=> x.Language == "es" && x.Tipo == FinancialStatesTypeEnum.Financial),
                 FinancialStates = _context.FinancialStates.FirstOrDefault()
            };

            return View(model);
        }

        public async Task<ActionResult> Cifras(string item = "", int year = 0, int quarter = 0)
        {
           
            ICifrasViewModel model;

            var periodos = QuarterHelper.getPeriods();
            var periodoActual = periodos.Where(x => x.Year == year && x.Quarter == quarter).FirstOrDefault();

            if (periodoActual == null)
                periodoActual = periodos.First();

            switch (item)
            {
                case "ACRyP":
                    model = await getacrypVMAsync(periodoActual);
                    break;

                case "ADC":
                    model = await getADCVMAsync(periodoActual);
                    break;

                case "custodia":
                    model = await getcustodiaVMAsync(periodoActual);
                    break;

                case "otros":
                    model = await getotrosVMAsync(periodoActual);
                    break;

                default:
                    model = await getCifrasVMAsync(periodoActual);
                    break;
            }
            model.Periodos = periodos;
            model.Menu = getMenu();
           
            model.PageData = await _context.FinancialStatesModels.FirstOrDefaultAsync(x => x.Language == "es" && x.Tipo == FinancialStatesTypeEnum.Numbers);

            return View(model);
        }
        private async Task<ICifrasViewModel> getCifrasVMAsync(Period periodo)
        {
            RelevantData relevantData = await _context.RelevantData.FirstOrDefaultAsync(x => x.Year == periodo.Year && x.Quarter == periodo.Quarter);

            var model = new CifrasViewModel<DatosRelevantesViewModel>();

            model.MenuActivo = "datos";
            model.PeriodoActual = periodo;

            model.Data = new DatosRelevantesViewModel();
            model.Data.RelevantData = relevantData;
            model.Data.RelevantData.GraphData = relevantData.GraphData;

            var evolution = await _context.RelevantData.OrderBy(x => x.Year).OrderBy(x => x.Quarter).Skip(Math.Max(0, _context.RelevantData.Count() - 5)).ToListAsync();

            model.Data.Evolucion = evolution
                .Select(x => new BarGraphData { trimestre = QuarterHelper.GetShortString(x.Year, x.Quarter), cantidad = Convert.ToDecimal(x.SubAccounts) })
                .ToList();

            string tactual = QuarterHelper.GetShortString(periodo.Year, periodo.Quarter);

            model.Data.Evolucion.First(x=> x.trimestre == tactual).color = "#16a8ed";

            return model;
        }

        private async Task<ICifrasViewModel> getacrypVMAsync(Period periodo)
        {
            ACRyPData data = await _context.ACRyPData.FirstOrDefaultAsync(x => x.Year == periodo.Year && x.Quarter == periodo.Quarter);

            var model = new CifrasViewModel<ACRyPData>();

            model.MenuActivo = "ACRyP";
            model.PeriodoActual = periodo;

            model.Data = data;
            return model;
        }

        private async Task<ICifrasViewModel> getADCVMAsync(Period periodo)
        {
            List<ADCData> data = await _context.ADCData.OrderBy(x=> x.Year).OrderBy(x=> x.Quarter).Skip(Math.Max(0, _context.ADCData.Count() -5)).ToListAsync() ;

            var model = new CifrasViewModel<ADCViewModel>();

            model.MenuActivo = "ADC";
            model.PeriodoActual = periodo;

            var graph = new List<BarGraphData>
            {
                new BarGraphData{ trimestre = "1er T 2018", cantidad = 4505689},
                new BarGraphData{ trimestre = "2dor T 2018", cantidad = 340562, color = "#16a8ed"},
            };

            model.Data = new ADCViewModel {
                CustodiaInternacional = data.Select(x=> new BarGraphData { trimestre = QuarterHelper.GetShortString(x.Year, x.Quarter), cantidad = Convert.ToDecimal(x.SafeIntl)}).ToList(),
                CustodiaValorizada = data.Select(x => new BarGraphData { trimestre = QuarterHelper.GetShortString(x.Year, x.Quarter), cantidad = Convert.ToDecimal(x.SaveAmount) }).ToList(),
                CustodiaVN = data.Select(x => new BarGraphData { trimestre = QuarterHelper.GetShortString(x.Year, x.Quarter), cantidad = Convert.ToDecimal(x.SafeVN) }).ToList(),
                ADCData = data.FirstOrDefault(x=> x.Year == periodo.Year && x.Quarter == periodo.Quarter)
            };

            model.Data.CustodiaInternacional.Last().color = "#1fbc2e ";
            model.Data.CustodiaValorizada.Last().color = "#1bb4b5 ";
            model.Data.CustodiaVN.Last().color = "#16a8ed";

            return model;
        }

        private async Task<ICifrasViewModel> getcustodiaVMAsync(Period periodo)
        {
            ActivosEnCustodia activosEnCustodia = await _context.ActivosEnCustodia.FirstOrDefaultAsync(x => x.Year == periodo.Year && x.Quarter == periodo.Quarter);
            var model = new CifrasViewModel<ActivosEnCustodia>();

            model.MenuActivo = "custodia";
            model.PeriodoActual = periodo;

            model.Data = activosEnCustodia;
            return model;
        }

        private async Task<ICifrasViewModel> getotrosVMAsync(Period periodo)
        {
            OtrosServicios data = await _context.OtrosServicios.FirstOrDefaultAsync(x => x.Year == periodo.Year && x.Quarter == periodo.Quarter);

            var model = new CifrasViewModel<OtrosServicios>();

            model.MenuActivo = "otros";
            model.PeriodoActual = periodo;
            model.Data = data;

            return model;
        }

        private List<MenuViewModel> getMenu()
        {
            return new List<MenuViewModel>
            {
                new MenuViewModel{ Text = "Estados financieros", Value = "-",  Link = "/InfoFin/Estados" },
                new MenuViewModel{ Text = "Datos Relevantes", Value = "datos", Link = "/InfoFin/Cifras?item=" },
                new MenuViewModel{ Text = "Servicios como ACRyP", Value = "ACRyP", Link = "/InfoFin/Cifras?item=ACRyP" },
                new MenuViewModel{ Text = "Servicios como ADC", Value = "ADC", Link = "/InfoFin/Cifras?item=ADC" },
                new MenuViewModel{ Text = "Participación de activos en total de custodia", Value = "custodia", Link = "/InfoFin/Cifras?item=custodia" },
                new MenuViewModel{ Text = "Depósito internacional de valores negociables", Value = "deposito", Link = "/InfoFin/Cifras?item=deposito" },
                new MenuViewModel{ Text = "Otros Servicios", Value = "otros", Link = "/InfoFin/Cifras?item=otros" },
            };
        }
    }
}