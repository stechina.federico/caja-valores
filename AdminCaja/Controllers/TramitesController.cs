﻿using AdminCaja.Models;
using AdminCaja.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AdminCaja.Controllers
{
    public class TramitesController : BaseController
    {
        private ApplicationDbContext _context;

        public TramitesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Tramites
        public async Task<ActionResult> Index()
        {
            var model = await getVM();

            return View(model);
        }

        public async Task<ActionResult> GetData(int tipoCliente = 0, int tipoTramite = 0, int tramiteId = 0)
        {
            var model = await getVM(tipoCliente, tipoTramite, tramiteId);

            return PartialView("_panelFiltro",model);
        }

        public async Task<ActionResult> Search(string q)
        {
            var list = await _context.Procedures.Where(x => x.Name.Contains(q)).Take(10).OrderBy(x => x.Name).Select(x => new { id = x.Id, name = x.Name }).ToListAsync();

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        private async Task<TramitesViewModel> getVM(int tipoCliente = 0 , int tipoTramite = 0, int tramiteId = 0)
        {
            TramitesViewModel model = new TramitesViewModel();

            model.SelectedTipoCliente = tipoCliente;
            model.SelectedTipoTramite = tipoTramite;
            model.SelectedTramite = tramiteId;
            model.TiposCliente = await GetCustomerTypes();
            model.TiposTramite = await GetProcedureTypes(model.SelectedTipoCliente);
            model.Tramites = await GetProcedures(model.SelectedTipoTramite);

            if(model.SelectedTramite != 0 && model.Tramites.Count() >0)
            {
                var t = model.Tramites.First();
                model.SelectedTipoCliente = t.ProcedureType.CustomerTypeId;
                model.SelectedTipoTramite = t.ProcedureTypeId;
            }

            return model;
        }

        private async Task<IEnumerable<SelectListItem>> GetCustomerTypes()
        {
            return await _context.CustomerTypes.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToListAsync();
        }

        private async Task<IEnumerable<SelectListItem>> GetProcedureTypes(int CustomerTypeId)
        {
            var r = _context.ProcedureTypes.Select(x=> x);

            if(CustomerTypeId != 0)
            {
                r = r.Where(x => x.CustomerTypeId == CustomerTypeId);
            }

            return await r.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToListAsync();
        }

        private async Task<IEnumerable<Procedure>> GetProcedures(int ProcedureTypeId)
        {
            var r = _context.Procedures.Select(x => x);

            if (ProcedureTypeId != 0)
            {
                r = r.Where(x => x.ProcedureTypeId == ProcedureTypeId);
            }

            return await r.ToListAsync();
        }

    }
}