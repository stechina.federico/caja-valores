﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminCaja.Areas.Admin.Models;
using AdminCaja.Areas.Admin.Models.Procedures;
using AdminCaja.Areas.Admin.Models.Section;
using AdminCaja.Areas.Admin.Models.Services;
using AdminCaja.Models;
using AutoMapper;

namespace AdminCaja.Helpers
{
    public class AutomapperProfiles: Profile
    {
        public AutomapperProfiles()
        {
            //Sections
            CreateMap<Section, SectionListViewModel>().ReverseMap();
            CreateMap<Section, SectionViewModel>()
                .ReverseMap();
            CreateMap<Content, ContentViewModel>()
                .ForMember(x=> x.FileName, opt=> opt.MapFrom((src, dest, destMember, Context) => src.Image ))
                .ForMember(x => x.HasDocuments, opt => opt.MapFrom((src, dest, destMember, Context) => src.Section.HasDocuments))
                .ForMember(x => x.HasContent, opt => opt.MapFrom((src, dest, destMember, Context) => src.Section.HasContent))
                .ReverseMap();

            //Services
            CreateMap<Service, ServiceViewModel>().ReverseMap();
            CreateMap<Service, ServiceListViewModel>().ReverseMap();
            CreateMap<ServiceContent, ServiceContentViewModel>()
                .ForMember(x=> x.ServiceTitle, o=> o.MapFrom(_=> _.Service.Title))
                .ReverseMap()
                .ForMember(x=> x.Service, o=> o.Ignore());
            CreateMap<ServiceContent, ServiceContentListViewModel>().ReverseMap();

            //Procedures
            CreateMap<ProcedureType, ProcedureTypeViewModel>()
                .ForMember(x=> x.CustomerType, op=> op.MapFrom(_=> _.CustomerType.Name))
                .ReverseMap()
                .ForMember(x=> x.CustomerType, op=> op.Ignore());

            CreateMap<Procedure, ProcedureViewModel>()
                .ForMember(x => x.ProcedureTypeCustomerTypeName, op => op.MapFrom(_ => _.ProcedureType.CustomerType.Name))
                .ForMember(x=> x.Services, op => op.Ignore())
                .ReverseMap()
                .ForMember(x => x.ProcedureType, op => op.Ignore())
                .ForMember(x => x.Services, op => op.Ignore());

            CreateMap<ProcedureFile, ProcedureFileViewModel>().ReverseMap();

            CreateMap<SubSection, SubSectionListViewModel>().ReverseMap();
        }
    }

}