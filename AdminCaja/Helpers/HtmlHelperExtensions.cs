﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AdminCaja.Helpers
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString CustomCheckBoxFor<TModel, TValue>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TValue>> expression)
        {
            //get the data from the model binding
            var fieldName = ExpressionHelper.GetExpressionText(expression);
            var fullBindingName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(fieldName);
            var fieldId = TagBuilder.CreateSanitizedId(fullBindingName);
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var modelValue = metaData.Model;

            //create the checkbox
            TagBuilder checkbox = new TagBuilder("input");
            checkbox.MergeAttribute("type", "checkbox");
            checkbox.MergeAttribute("value", "true"); //the visible checkbox must always have true
            checkbox.MergeAttribute("name", fullBindingName);
            checkbox.MergeAttribute("id", fieldId);

            //is the checkbox checked
            bool isChecked = false;
            if (modelValue != null)
            {
                bool.TryParse(modelValue.ToString(), out isChecked);
            }
            if (isChecked)
            {
                checkbox.MergeAttribute("checked", "checked");
            }

            //add the validation
            checkbox.MergeAttributes(htmlHelper.GetUnobtrusiveValidationAttributes(fieldId, metaData));

            //create the label in the outer div
            var label = new TagBuilder("label");
            label.MergeAttribute("for", fieldId);
            label.AddCssClass("switch");

            //render the control
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(label.ToString(TagRenderMode.StartTag));
            sb.AppendLine(checkbox.ToString(TagRenderMode.SelfClosing));
            //sb.AppendLine(labelText); //the label
            sb.AppendLine("<span class=\"slider round\"></span>");
            sb.AppendLine(label.ToString(TagRenderMode.EndTag));

            //create the extra hidden input needed by MVC outside the div
            TagBuilder hiddenCheckbox = new TagBuilder("input");
            hiddenCheckbox.MergeAttribute("type", HtmlHelper.GetInputTypeString(InputType.Hidden));
            hiddenCheckbox.MergeAttribute("name", fullBindingName);
            hiddenCheckbox.MergeAttribute("value", "false");
            sb.Append(hiddenCheckbox.ToString(TagRenderMode.SelfClosing));

            //return the custom checkbox
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString SideBarActionLink(this HtmlHelper htmlHelper, string linkText, string iconClass, string controller, string action)
        {
            return SideBarActionLink(htmlHelper, linkText, iconClass, controller, action, null, null);
        }

        public static MvcHtmlString SideBarActionLink(this HtmlHelper htmlHelper, string linkText, string iconClass, string controller, string action, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            var actionLink = new TagBuilder("a");
            UrlHelper urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);

            actionLink.Attributes["href"] = urlHelper.Action(action, controller, routeValues);
            actionLink.InnerHtml = "<i class=\"" + iconClass + "\"></i><span class=\"hide-menu\">" + linkText + "</span>";

            if(htmlAttributes!=null)
                actionLink.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            return MvcHtmlString.Create(actionLink.ToString());
        }

        public static MvcHtmlString CountNumber(this HtmlHelper htmlHelper, int valor)
        {
            return CountNumber(htmlHelper, valor.ToString("#,###", CultureInfo.InvariantCulture));
        }

        public static MvcHtmlString CountNumber(this HtmlHelper htmlHelper, decimal valor)
        {
            return CountNumber(htmlHelper, valor.ToString("#,###.##", CultureInfo.InvariantCulture));
        }

        public static MvcHtmlString CountNumber(this HtmlHelper htmlHelper, string valor)
        {
            if (valor.Contains('.'))
            {
                var parts = valor.Split('.');

                return MvcHtmlString.Create(CreateSpans(parts[0].Split(',')) + "," + CreateSpan(parts[1]));

            }
            else
            {
                return MvcHtmlString.Create(CreateSpans(valor.Split(',')));
            }
          
        }

        private static string CreateSpan(string valor)
        {
            var tb = new TagBuilder("span");
            tb.AddCssClass("count");
            tb.SetInnerText(valor);
            return tb.ToString(TagRenderMode.Normal);
        }

        private static string CreateSpans(string[] numbers)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < numbers.Count(); i++)
            {
                sb.Append(CreateSpan(numbers[i]));
                if (i != numbers.Count()-1)
                    sb.Append(".");
            }

            return sb.ToString();
        }

    }
}