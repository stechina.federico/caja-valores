﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminCaja.Helpers
{
    public static class QuarterHelper
    {
        public static string[] Ordinal= new string[] { "1er", "2do", "3er", "4to" };

        public static List<Period> getPeriods()
        {
            List<Period> result = new List<Period>();

            int inicio = (DateTime.Now.Month + 2) / 3;
            int año = DateTime.Now.Year;

            for (int i = inicio; i > inicio - 4; i--)
            {
                if (i > 0)
                {
                    result.Add(new Period { Quarter = i, Year = año, Name = $"{Ordinal[i-1]} Trimestre {año}" });
                }
                else
                {
                    result.Add(new Period { Quarter = 4 - i, Year = año - 1, Name = $"{Ordinal[3 - i]} Trimestre {año - 1}" });
                }

            }

            return result;
        }
        public static string GetShortString(int year, int quarter)
        {
            return $"{quarter}{Ordinal[quarter]} {year}";
        }

    }

    public class Period
    {
        public int Year { get; set; }
        public int Quarter { get; set; }
        public string Name { get; set; }
    }

}