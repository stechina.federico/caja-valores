﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AdminCaja.Startup))]
namespace AdminCaja
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            InitializeContainer(app);
            ConfigureAuth(app);
        }
    }
}
