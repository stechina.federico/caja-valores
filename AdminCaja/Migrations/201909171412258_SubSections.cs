namespace AdminCaja.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SubSections : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SubSections", "Type", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SubSections", "Type");
        }
    }
}
