namespace AdminCaja.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Services : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        HoverText = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ServiceContents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Body = c.String(),
                        Language = c.String(),
                        ServiceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Services", t => t.ServiceId, cascadeDelete: true)
                .Index(t => t.ServiceId);
            
            CreateTable(
                "dbo.ServiceProcedures",
                c => new
                    {
                        Service_Id = c.Int(nullable: false),
                        Procedure_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Service_Id, t.Procedure_Id })
                .ForeignKey("dbo.Services", t => t.Service_Id, cascadeDelete: true)
                .ForeignKey("dbo.Procedures", t => t.Procedure_Id, cascadeDelete: true)
                .Index(t => t.Service_Id)
                .Index(t => t.Procedure_Id);
            
            AddColumn("dbo.Procedures", "WhatIsIt", c => c.String());
            AddColumn("dbo.Procedures", "ForWhom", c => c.String());
            AddColumn("dbo.Procedures", "HowTo", c => c.String());
            AddColumn("dbo.Procedures", "WhereIs", c => c.String());
            AddColumn("dbo.Procedures", "Area", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ServiceProcedures", "Procedure_Id", "dbo.Procedures");
            DropForeignKey("dbo.ServiceProcedures", "Service_Id", "dbo.Services");
            DropForeignKey("dbo.ServiceContents", "ServiceId", "dbo.Services");
            DropIndex("dbo.ServiceProcedures", new[] { "Procedure_Id" });
            DropIndex("dbo.ServiceProcedures", new[] { "Service_Id" });
            DropIndex("dbo.ServiceContents", new[] { "ServiceId" });
            DropColumn("dbo.Procedures", "Area");
            DropColumn("dbo.Procedures", "WhereIs");
            DropColumn("dbo.Procedures", "HowTo");
            DropColumn("dbo.Procedures", "ForWhom");
            DropColumn("dbo.Procedures", "WhatIsIt");
            DropTable("dbo.ServiceProcedures");
            DropTable("dbo.ServiceContents");
            DropTable("dbo.Services");
        }
    }
}
