namespace AdminCaja.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Finantial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ACRyPDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        Quarter = c.Int(nullable: false),
                        CreditsAmount = c.Int(nullable: false),
                        CreditsPesos = c.Decimal(nullable: false, precision: 18, scale: 2),
                        JudicialOffices = c.Int(nullable: false),
                        RegisteredAccounts = c.Int(nullable: false),
                        People = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ActivosEnCustodias",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        Quarter = c.Int(nullable: false),
                        Totals_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TotalesAECs", t => t.Totals_Id)
                .Index(t => t.Totals_Id);
            
            CreateTable(
                "dbo.DetailedAECs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ColName = c.String(),
                        ActivosEnCustodia_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ActivosEnCustodias", t => t.ActivosEnCustodia_Id)
                .Index(t => t.ActivosEnCustodia_Id);
            
            CreateTable(
                "dbo.TotalesAECs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TotalVPN = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ADCDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        Quarter = c.Int(nullable: false),
                        SafeVN = c.Int(nullable: false),
                        SaveAmount = c.Int(nullable: false),
                        SafeIntl = c.Int(nullable: false),
                        Kind8Amount = c.Int(nullable: false),
                        Kind10Amount = c.Int(nullable: false),
                        Kind7Amount = c.Int(nullable: false),
                        Kind6Amount = c.Int(nullable: false),
                        Kind8 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Kind10 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Kind7 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Kind6 = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DepositoInternacionals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        Month = c.Int(nullable: false),
                        Euroclear = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ClearStream = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DTC = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CBLC = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Iberclear = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OtrosServiciosDataDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OtrosServicios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        Quarter = c.Int(nullable: false),
                        Letters = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CPD_Id = c.Int(),
                        PagareBursatil_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OtrosServiciosDatas", t => t.CPD_Id)
                .ForeignKey("dbo.OtrosServiciosDatas", t => t.PagareBursatil_Id)
                .Index(t => t.CPD_Id)
                .Index(t => t.PagareBursatil_Id);
            
            CreateTable(
                "dbo.OtrosServiciosDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CasaCentralPesos_Id = c.Int(),
                        CasaCentralUs_Id = c.Int(),
                        SucursalesPesos_Id = c.Int(),
                        SucursalesUs_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OtrosServiciosDataDetails", t => t.CasaCentralPesos_Id)
                .ForeignKey("dbo.OtrosServiciosDataDetails", t => t.CasaCentralUs_Id)
                .ForeignKey("dbo.OtrosServiciosDataDetails", t => t.SucursalesPesos_Id)
                .ForeignKey("dbo.OtrosServiciosDataDetails", t => t.SucursalesUs_Id)
                .Index(t => t.CasaCentralPesos_Id)
                .Index(t => t.CasaCentralUs_Id)
                .Index(t => t.SucursalesPesos_Id)
                .Index(t => t.SucursalesUs_Id);
            
            CreateTable(
                "dbo.RelevantDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        Quarter = c.Int(nullable: false),
                        SubAccounts = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RelevantDataGraphs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Percent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RelevantData_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RelevantDatas", t => t.RelevantData_Id)
                .Index(t => t.RelevantData_Id);
            
            AddColumn("dbo.FinancialStatesGraphs", "DetailedAEC_Id", c => c.Int());
            AddColumn("dbo.FinancialStatesGraphs", "TotalesAEC_Id", c => c.Int());
            CreateIndex("dbo.FinancialStatesGraphs", "DetailedAEC_Id");
            CreateIndex("dbo.FinancialStatesGraphs", "TotalesAEC_Id");
            AddForeignKey("dbo.FinancialStatesGraphs", "DetailedAEC_Id", "dbo.DetailedAECs", "Id");
            AddForeignKey("dbo.FinancialStatesGraphs", "TotalesAEC_Id", "dbo.TotalesAECs", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RelevantDataGraphs", "RelevantData_Id", "dbo.RelevantDatas");
            DropForeignKey("dbo.OtrosServicios", "PagareBursatil_Id", "dbo.OtrosServiciosDatas");
            DropForeignKey("dbo.OtrosServicios", "CPD_Id", "dbo.OtrosServiciosDatas");
            DropForeignKey("dbo.OtrosServiciosDatas", "SucursalesUs_Id", "dbo.OtrosServiciosDataDetails");
            DropForeignKey("dbo.OtrosServiciosDatas", "SucursalesPesos_Id", "dbo.OtrosServiciosDataDetails");
            DropForeignKey("dbo.OtrosServiciosDatas", "CasaCentralUs_Id", "dbo.OtrosServiciosDataDetails");
            DropForeignKey("dbo.OtrosServiciosDatas", "CasaCentralPesos_Id", "dbo.OtrosServiciosDataDetails");
            DropForeignKey("dbo.ActivosEnCustodias", "Totals_Id", "dbo.TotalesAECs");
            DropForeignKey("dbo.FinancialStatesGraphs", "TotalesAEC_Id", "dbo.TotalesAECs");
            DropForeignKey("dbo.DetailedAECs", "ActivosEnCustodia_Id", "dbo.ActivosEnCustodias");
            DropForeignKey("dbo.FinancialStatesGraphs", "DetailedAEC_Id", "dbo.DetailedAECs");
            DropIndex("dbo.RelevantDataGraphs", new[] { "RelevantData_Id" });
            DropIndex("dbo.OtrosServiciosDatas", new[] { "SucursalesUs_Id" });
            DropIndex("dbo.OtrosServiciosDatas", new[] { "SucursalesPesos_Id" });
            DropIndex("dbo.OtrosServiciosDatas", new[] { "CasaCentralUs_Id" });
            DropIndex("dbo.OtrosServiciosDatas", new[] { "CasaCentralPesos_Id" });
            DropIndex("dbo.OtrosServicios", new[] { "PagareBursatil_Id" });
            DropIndex("dbo.OtrosServicios", new[] { "CPD_Id" });
            DropIndex("dbo.FinancialStatesGraphs", new[] { "TotalesAEC_Id" });
            DropIndex("dbo.FinancialStatesGraphs", new[] { "DetailedAEC_Id" });
            DropIndex("dbo.DetailedAECs", new[] { "ActivosEnCustodia_Id" });
            DropIndex("dbo.ActivosEnCustodias", new[] { "Totals_Id" });
            DropColumn("dbo.FinancialStatesGraphs", "TotalesAEC_Id");
            DropColumn("dbo.FinancialStatesGraphs", "DetailedAEC_Id");
            DropTable("dbo.RelevantDataGraphs");
            DropTable("dbo.RelevantDatas");
            DropTable("dbo.OtrosServiciosDatas");
            DropTable("dbo.OtrosServicios");
            DropTable("dbo.OtrosServiciosDataDetails");
            DropTable("dbo.DepositoInternacionals");
            DropTable("dbo.ADCDatas");
            DropTable("dbo.TotalesAECs");
            DropTable("dbo.DetailedAECs");
            DropTable("dbo.ActivosEnCustodias");
            DropTable("dbo.ACRyPDatas");
        }
    }
}
