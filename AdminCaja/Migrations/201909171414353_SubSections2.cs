namespace AdminCaja.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SubSections2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SubSections", "Title", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SubSections", "Title", c => c.String(maxLength: 25));
        }
    }
}
