namespace AdminCaja.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FileName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Files", "FileName", c => c.String());
            AddColumn("dbo.ProcedureFiles", "FileName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProcedureFiles", "FileName");
            DropColumn("dbo.Files", "FileName");
        }
    }
}
