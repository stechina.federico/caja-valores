namespace AdminCaja.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SubSections3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contents", "ContentEnabled", c => c.Boolean(nullable: false));
            AddColumn("dbo.Sections", "HasContent", c => c.Boolean(nullable: false));
            AddColumn("dbo.Sections", "HasDocuments", c => c.Boolean(nullable: false));
            DropColumn("dbo.Contents", "HasContent");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contents", "HasContent", c => c.Boolean(nullable: false));
            DropColumn("dbo.Sections", "HasDocuments");
            DropColumn("dbo.Sections", "HasContent");
            DropColumn("dbo.Contents", "ContentEnabled");
        }
    }
}
