namespace AdminCaja.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DefaultDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 25),
                        Image = c.String(),
                        HasContent = c.Boolean(nullable: false),
                        Body = c.String(),
                        Language = c.String(),
                        SectionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sections", t => t.SectionId, cascadeDelete: true)
                .Index(t => t.SectionId);
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Path = c.String(),
                        Language = c.String(),
                        ContentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contents", t => t.ContentId, cascadeDelete: true)
                .Index(t => t.ContentId);
            
            CreateTable(
                "dbo.Sections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 25),
                        IsPublic = c.Boolean(nullable: false),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SubSections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SectionId = c.Int(nullable: false),
                        Title = c.String(maxLength: 25),
                        Image = c.String(),
                        Language = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sections", t => t.SectionId, cascadeDelete: true)
                .Index(t => t.SectionId);
            
            CreateTable(
                "dbo.CustomerTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DocFiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Path = c.String(),
                        ProcedureId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Procedures", t => t.ProcedureId, cascadeDelete: true)
                .Index(t => t.ProcedureId);
            
            CreateTable(
                "dbo.Procedures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcedureTypeId = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProcedureTypes", t => t.ProcedureTypeId, cascadeDelete: true)
                .Index(t => t.ProcedureTypeId);
            
            CreateTable(
                "dbo.FormFiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Path = c.String(),
                        ProcedureId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Procedures", t => t.ProcedureId, cascadeDelete: true)
                .Index(t => t.ProcedureId);
            
            CreateTable(
                "dbo.ProcedureTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CustomerTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomerTypes", t => t.CustomerTypeId, cascadeDelete: true)
                .Index(t => t.CustomerTypeId);
            
            CreateTable(
                "dbo.Slides",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Order = c.Int(nullable: false),
                        Title = c.String(),
                        Drop = c.String(),
                        Image = c.String(),
                        ButtonCaption = c.String(),
                        ButtonUrl = c.String(),
                        Language = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Procedures", "ProcedureTypeId", "dbo.ProcedureTypes");
            DropForeignKey("dbo.ProcedureTypes", "CustomerTypeId", "dbo.CustomerTypes");
            DropForeignKey("dbo.FormFiles", "ProcedureId", "dbo.Procedures");
            DropForeignKey("dbo.DocFiles", "ProcedureId", "dbo.Procedures");
            DropForeignKey("dbo.SubSections", "SectionId", "dbo.Sections");
            DropForeignKey("dbo.Contents", "SectionId", "dbo.Sections");
            DropForeignKey("dbo.Files", "ContentId", "dbo.Contents");
            DropIndex("dbo.ProcedureTypes", new[] { "CustomerTypeId" });
            DropIndex("dbo.FormFiles", new[] { "ProcedureId" });
            DropIndex("dbo.Procedures", new[] { "ProcedureTypeId" });
            DropIndex("dbo.DocFiles", new[] { "ProcedureId" });
            DropIndex("dbo.SubSections", new[] { "SectionId" });
            DropIndex("dbo.Files", new[] { "ContentId" });
            DropIndex("dbo.Contents", new[] { "SectionId" });
            DropTable("dbo.Slides");
            DropTable("dbo.ProcedureTypes");
            DropTable("dbo.FormFiles");
            DropTable("dbo.Procedures");
            DropTable("dbo.DocFiles");
            DropTable("dbo.CustomerTypes");
            DropTable("dbo.SubSections");
            DropTable("dbo.Sections");
            DropTable("dbo.Files");
            DropTable("dbo.Contents");
        }
    }
}
