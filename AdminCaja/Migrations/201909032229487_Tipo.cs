namespace AdminCaja.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Tipo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FinancialStatesModels", "Tipo", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FinancialStatesModels", "Tipo");
        }
    }
}
