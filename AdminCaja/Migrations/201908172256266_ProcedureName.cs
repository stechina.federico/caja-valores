namespace AdminCaja.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProcedureName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Procedures", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Procedures", "Name");
        }
    }
}
