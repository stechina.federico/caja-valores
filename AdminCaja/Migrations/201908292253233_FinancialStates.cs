namespace AdminCaja.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FinancialStates : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FinancialStates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        SocialCapital = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NetWorth = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FinancialStatesGraphs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FinancialStates_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FinancialStates", t => t.FinancialStates_Id)
                .Index(t => t.FinancialStates_Id);
            
            CreateTable(
                "dbo.FinancialStatesModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        HeaderImage = c.String(),
                        Language = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FinancialBalances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        Name = c.String(),
                        Path = c.String(),
                        FileName = c.String(),
                        Language = c.String(),
                        Order = c.Int(nullable: false),
                        FinancialStatesModel_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FinancialStatesModels", t => t.FinancialStatesModel_Id)
                .Index(t => t.FinancialStatesModel_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FinancialBalances", "FinancialStatesModel_Id", "dbo.FinancialStatesModels");
            DropForeignKey("dbo.FinancialStatesGraphs", "FinancialStates_Id", "dbo.FinancialStates");
            DropIndex("dbo.FinancialBalances", new[] { "FinancialStatesModel_Id" });
            DropIndex("dbo.FinancialStatesGraphs", new[] { "FinancialStates_Id" });
            DropTable("dbo.FinancialBalances");
            DropTable("dbo.FinancialStatesModels");
            DropTable("dbo.FinancialStatesGraphs");
            DropTable("dbo.FinancialStates");
        }
    }
}
