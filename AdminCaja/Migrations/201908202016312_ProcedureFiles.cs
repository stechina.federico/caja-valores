namespace AdminCaja.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProcedureFiles : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DocFiles", "ProcedureId", "dbo.Procedures");
            DropForeignKey("dbo.FormFiles", "ProcedureId", "dbo.Procedures");
            DropIndex("dbo.DocFiles", new[] { "ProcedureId" });
            DropIndex("dbo.FormFiles", new[] { "ProcedureId" });
            CreateTable(
                "dbo.ProcedureFiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcedureId = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        Name = c.String(),
                        Path = c.String(),
                        Language = c.String(),
                        Order = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Procedures", t => t.ProcedureId, cascadeDelete: true)
                .Index(t => t.ProcedureId);
            
            AddColumn("dbo.Files", "Order", c => c.Int(nullable: false));
            DropTable("dbo.DocFiles");
            DropTable("dbo.FormFiles");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.FormFiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Path = c.String(),
                        ProcedureId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DocFiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Path = c.String(),
                        ProcedureId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.ProcedureFiles", "ProcedureId", "dbo.Procedures");
            DropIndex("dbo.ProcedureFiles", new[] { "ProcedureId" });
            DropColumn("dbo.Files", "Order");
            DropTable("dbo.ProcedureFiles");
            CreateIndex("dbo.FormFiles", "ProcedureId");
            CreateIndex("dbo.DocFiles", "ProcedureId");
            AddForeignKey("dbo.FormFiles", "ProcedureId", "dbo.Procedures", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DocFiles", "ProcedureId", "dbo.Procedures", "Id", cascadeDelete: true);
        }
    }
}
