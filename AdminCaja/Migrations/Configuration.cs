namespace AdminCaja.Migrations
{
    using AdminCaja.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.Diagnostics;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AdminCaja.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AdminCaja.Models.ApplicationDbContext context)
        {
            try
            {

                if (!context.Roles.Any(r => r.Name == "Administrators"))
                {
                    var store = new RoleStore<IdentityRole>(context);
                    var manager = new RoleManager<IdentityRole>(store);
                    var role = new IdentityRole { Name = "Administrators" };

                    manager.Create(role);
                }

                if (!context.Users.Any(u => u.UserName == "Admin@admin.com"))
                {
                    var store = new UserStore<ApplicationUser>(context);
                    var manager = new UserManager<ApplicationUser>(store);
                    var user = new ApplicationUser { UserName = "Admin@admin.com", Email = "Admin@admin.com" , EmailConfirmed = true };

                    manager.Create(user, "CajaDeValores");
                    manager.AddToRole(user.Id, "Administrators");
                }

                if(context.CustomerTypes.Count() == 0)
                {
                    context.CustomerTypes.Add(new CustomerType { Name = "Depositante" });
                    context.CustomerTypes.Add(new CustomerType { Name = "Inversores" });
                    context.CustomerTypes.Add(new CustomerType { Name = "Emisor" });
                    context.CustomerTypes.Add(new CustomerType { Name = "Titular de cuenta en el registro" });
                    context.CustomerTypes.Add(new CustomerType { Name = "Responsable CHPD / Pagar�s" });
                    context.CustomerTypes.Add(new CustomerType { Name = "Otros tr�mites" });

                    context.SaveChanges();
                }

                if(context.Sections.Count() == 0)
                {
                    //Custom, Gobierno, Comunicados, Novedades

                    context.Sections.Add(new Section {
                        Name = "Caja de Valores",
                        Type = Models.SectionType.Caja,
                        SubSections = new System.Collections.ObjectModel.Collection<SubSection>
                        {
                            new SubSection{ Language = "es", Title = "�Que es caja de Valores?", Type = Models.SubSectionType.QueEs},
                            new SubSection {Language = "es", Title = "Carta del Presidente", Type = Models.SubSectionType.CartaPresidente },
                            new SubSection {Language = "es", Title = "Mision, visi�n, valores", Type = Models.SubSectionType.Mision },
                            new SubSection {Language = "es", Title = "Marco legal", Type = Models.SubSectionType.MarcoLegal },
                            new SubSection {Language = "es", Title = "Marco Internacional", Type = Models.SubSectionType.MarcoInternacional },
                            new SubSection {Language = "es", Title = "Trabaja con Nosotros", Type = Models.SubSectionType.Trabajo }
                        }
                    });

                    context.Sections.Add(new Section {
                        Name = "Gobierno corporativo",
                        Type = Models.SectionType.Gobierno,
                        SubSections = new System.Collections.ObjectModel.Collection<SubSection>
                        {
                            new SubSection{ Language = "es", Title = "Directorio", Type = Models.SubSectionType.Directorio},
                            new SubSection{ Language = "es", Title = "Organigrama", Type = Models.SubSectionType.Organigrama},
                            new SubSection{ Language = "es", Title = "C�digo de conducta y de �tica", Type = Models.SubSectionType.Codigos},
                            new SubSection{ Language = "es", Title = "Sustentabilidad", Type = Models.SubSectionType.Sustentabilidad},
                            new SubSection{ Language = "es", Title = "Programa de integridad", Type = Models.SubSectionType.Integridad},

                        }
                    });
                    context.Sections.Add(new Section { Name = "Comunicados y circulares", Type = Models.SectionType.Comunicados });
                    context.Sections.Add(new Section { Name = "Novedades", Type = Models.SectionType.Novedades });

                    //Crea las versiones en Ingles
                    foreach (var item in context.Sections)
                    {
                        if(item.SubSections != null)
                        {
                            foreach (var sub in item.SubSections)
                            {
                                item.SubSections.Add(new SubSection { Language = "en", Title = sub.Title, Type = sub.Type });
                            }
                        }

                    }
                }

                if(context.FinancialStatesModels.Count() == 0)
                {
                    context.FinancialStatesModels.Add(new FinancialStatesModel { Language = "es", HeaderImage = "/images/Upload/infofin.jpg", Title = "Estados financieros" });
                    context.FinancialStatesModels.Add(new FinancialStatesModel { Language = "en", HeaderImage = "/images/Upload/infofin.jpg", Title = "Finantial states" });
                }

            }catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join(";", errorMessages);
                Debug.WriteLine("Error: " + fullErrorMessage);
                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, "; The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new Exception(exceptionMessage, ex);

            }catch(Exception ex)
            {
                Debug.WriteLine("Error generico: " + ex.Message);

            }

        }
    }
}
